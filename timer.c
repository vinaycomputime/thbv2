/* 
Automatic Balancing Valve
File: timer.c
*/

#include	"AiP8F1016.h"
#include "main.h"

//************************************************************************************
//											BALANCING VALVE CALLBACK TIMER CONTROL
//************************************************************************************



extern xdata TIMERITEM TimerList[4];
  
extern xdata int testIncr;
extern xdata int startCalib;
extern xdata int motorCurrVoltage[7];
extern xdata int motorCurrVoltageAvg;
extern xdata int motorCurrVAvgdbg;
extern xdata WORD mtrcountfree;	
	
void TimerPoll(void)
{
static BYTE idx;
clearWDT();

/*if(startCalib == 1)
{	
if(testIncr > 1){
	testIncr = 0;
	readMotorCurrentAD();
	//sendpin(motorCurrVAvgdbg);
	//sendpin(mtrcountfree);
}
}*/

for(idx=0;idx < 4;idx++)
	{
	if(TimerList[idx].timeout == TRUE)				// check for timeout
		{
		TimerList[idx].timeout = FALSE;					// OK then clear it
		if(TimerList[idx].callback != NULL) (*TimerList[idx].callback) ();	// OK go do the function callback
		if(TimerList[idx].value == 57601) // was it a one time?
			{
				TimerList[idx].callback=NULL;  // dont need it anymore
				TimerList[idx].msecs = 57601;
				TimerList[idx].value=0;
			}
		}
	}
}

void TimerStart(BYTE idx, void(*callbackFunction) (void), WORD msecs, BYTE mode)
{
TimerList[idx].callback = callbackFunction;
TimerList[idx].msecs = msecs;
TimerList[idx].value = 0;
TimerList[idx].mode = mode;
TimerList[idx].timeout = FALSE;
}

void TimerReset(BYTE idx)
{
	TimerList[idx].callback = NULL;
	TimerList[idx].timeout = FALSE;
	TimerList[idx].value = 0;
	TimerList[idx].mode = TIMER_ONE_TIME;
	TimerList[idx].msecs = 57601;
}


