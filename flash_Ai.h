#ifndef _FLASH_H_
#define _FLASH_H_

#include <Intrins.h>
#include <absacc.h>
#include "AiP8F1016.h"

typedef unsigned char uint8;
typedef unsigned int	uint16;
typedef unsigned long	uint32;



void PageErase(uint16 addr);
uint8 PageEraseVA(uint16 addr, uint16 verify_num);
uint8 PageEraseVB(uint16 addr, uint8 verify_num);
void ByteWrite(uint16 addr, uint8 dat);
uint8 ByteWriteV(uint16 addr, uint8 dat);
void ArrayWriteA(uint16 addr, uint8 xdata *ptr, uint16 num);
void ArrayWriteB(uint16 addr, uint8 xdata *ptr, uint8 num);
uint8 ArrayWriteVA(uint16 addr, uint8 xdata *ptr, uint16 num);
uint8 ArrayWriteVB(uint16 addr, uint8 xdata *ptr, uint8 num);
uint8 ByteRead(uint16 addr);


#endif