/* 
Automatic Balancing Valve
File: calibration.c

*/
#include	"AiP8F1016.h"
#include "main.h"

//************************************************************************************
//											BALANCING VALVE CALIBRATION
//************************************************************************************

/* It is here that we calibrate from the motor fully open count of the valve to find:
(1) Valve Opening Point (VOP), where the valve begins to allow water flow.
(2) Maximum Power Point (MPP), where opening the valve any further will have zero effect.

All balancing will take place ONLY between these two values (VOP & MPP).
For valves from the factory the calibration values and the cumulative on-time will be zero, therefore 
the software when first installed will run the calibration routines. It is expected that the valve 
should periodically recalibrate itself, for example the installer may refit the valve to a different 
position on the manifold or even transfer the flow and return sensors. The valve 'on-time' should be 
saved to provide a total time the valve has been opened, at each power down the valve will save in flash the 
cumulative on-time. After being on for > 84 (7*12) hours the calibration values and the on-time should be zeroed at 
the next off to force a re-calibration. Calibration can be done "on the fly" and the user will be unaware of 
any calibration taking place. Should the valve experience any serious abnormality the valve will zero the 
calibation & cumulative on-time values at the next off.
*/



