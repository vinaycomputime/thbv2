#include	"AiP8F1016.h"
#include	"func_def.h"

unsigned char ADCFlag;
unsigned char i;
unsigned int temp;
	
//vinay	
void ClearWDT(void)
{
 WDTCR |= 0x20;   // Clear the Watch-dog 
}

void WDTinit(void)
{
 // Set the Watch-dog timer
 WDTDR = 0xF0;     
 WDTCR = 0xC2;     
 ClearWDT();
}

void main()
{
	ADCFlag = 0;
	
	cli();          	// disable INT. during peripheral setting
	port_init();    	// initialize ports
	clock_init();   	// initialize operation clock
	ADC_init();     	// initialize A/D convertor
	UART_init();    	// initialize UART interface
	sei();          	// enable INT.

	IE |= 0x80;	
  LVRCR |=0x01;
	// TODO: add your main code here
	
	while(1)
	{
		 for(i=0; i<15; i++)
		 {
		 		ADC_start(15);
				while(ADCFlag==0);
				ADCFlag = 0;
				temp = ADC_read();
				//UART_SendData(temp); 
			  UART_write((temp>>8)&0x0f);
			  UART_write((temp&0xff));
		 }
		 //UART_write(0x0d);
		 //UART_write(0x0a);
	}
}

//======================================================
// interrupt routines
//======================================================

void INT_ADC() interrupt 18
{
	
	ADCFlag = 1;
	// ADC interrupt
	// TODO: add your code here
}

//======================================================
// peripheral setting routines
//======================================================

unsigned char UART_read()
{
	unsigned char dat;
	while(!(UARTST & 0x20));	// wait
	dat = UARTDR;   	// read
	return	dat;
}

unsigned int ADC_read()
{
	// read A/D convertor
	unsigned int adcVal;
	while(!(ADCCRL & 0x10));	// wait ADC busy
	adcVal = (ADCDRH << 8) | ADCDRL;	// read ADC
	return	adcVal;
}

void ADC_init()
{
	// initialize A/D convertor
	ADCCRL = 0x80;  	// setting
	ADCCRH = 0x04;  	// trigger source, alignment, frequency
	ADWCRL = 0x00;  	// Wake-up enable Low
	ADWCRH = 0x00;  	// Wake-up enable High
	ADWRCR0 = 0x00; 	// Wake-up R selection
	ADWRCR1 = 0x00; 	// Wake-up R selection
	ADWRCR2 = 0x00; 	// Wake-up R selection
	ADWRCR3 = 0x00; 	// Wake-up R selection
	IE3 |= 0x01;    	// enable ADC interrupt
}

void ADC_start(unsigned char ch)
{
	// start A/D convertor
	ADCCRL = (ADCCRL & 0xf0) | (ch & 0x0f);	// select channel
	ADCCRL |= 0x40; 	// start ADC
}

void UART_init()
{
	// initialize UART interface
	// ASync. 9615bps N 8 1
	UARTCR2 = 0x02; 	// activate UART
	UARTCR1 = 0x06; 	// bit count, parity
	UARTCR2 |= 0x0C;	// interrupt, speed
	UARTCR3 = 0x00; 	// stop bit
	UARTBD = 0x33;  	// baud rate
}

void UART_SendData(unsigned int dat)
{
	unsigned char temp1;
	unsigned char temp2;
	unsigned char temp3;

	temp3 = (dat>>8)&0x00ff;

	temp2 = temp3&0x0f;
	temp1 = (temp3>>4)&0x0f;
	if(temp1<10)
	{
		UART_write(temp1+0x30);
	}
	else
	{
		UART_write(temp1+0x37);
	}
	if(temp2<10)
	{
		UART_write(temp2+0x30);
	}
	else
	{
		UART_write(temp2+0x37);
	}

	UART_write(0x20);

	temp3 = dat&0x00ff;

	temp2 = temp3&0x0f;
	temp1 = (temp3>>4)&0x0f;
	if(temp1<10)
	{
		UART_write(temp1+0x30);
	}
	else
	{
		UART_write(temp1+0x37);
	}
	if(temp2<10)
	{
		UART_write(temp2+0x30);
	}
	else
	{
		UART_write(temp2+0x37);
	}
	UART_write(0x20);
}


void UART_write(unsigned char dat)
{
	while(!(UARTST & 0x80));	// wait
	UARTDR = dat;   	// write
}

void clock_init()
{
//	// internal RC clock (8.000000MHz)
	OSCCR = 0x20;   	// Set Int. OSC
	SCCR = 0x00;    	// Use Int. OSC
	// external clock
//	OSCCR = 0x0A;   	// Enable int. 1MHz and Ext. OSC
//	BITCR = 0x09;   	// Set waiting time : 16ms@1MHz
//	while((BITCR & 0x80) == 0);	// Ext. OSC stabilizing time
//	SCCR = 0x01;    	// Change to Ext. OSC
//	OSCCR |= 0x04;  	// Disable Int. OSC

}

// void clock_init()
// {
// 	// external clock
// 	OSCCR = 0x0A;   	// Enable int. 1MHz and Ext. OSC
// 	BITCR = 0x09;   	// Set waiting time : 16ms@1MHz
// 	while((BITCR & 0x80) == 0);	// Ext. OSC stabilizing time
// 	SCCR = 0x01;    	// Change to Ext. OSC
// 	OSCCR |= 0x04;  	// Disable Int. OSC
// }

void port_init()
{
	// initialize ports
	//   8 : RXD      in  
	//   9 : TXD      out 
	//  16 : AN14     in  
	//  17 : AN13     in  
	//  18 : AN12     in  
	//  19 : AN11     in  
	//  20 : AN10     in  
	//  21 : AN9      in  
	//  22 : AN8      in  
	//  23 : AN7      in  
	//  24 : AN3      in  
	//  25 : AN2      in  
	//  26 : AN1      in  
	//  27 : AN0      in  
	P0IO = 0xF0;    	// direction
	P0PU = 0x00;    	// pullup
	P0OD = 0x00;    	// open drain
	P03DB = 0x00;   	// bit7~6(debounce clock), bit5~0=P35,P06~02 debounce
	P0   = 0x00;    	// port initial value
	
	P1IO = 0x00;    	// direction
	P1PU = 0x00;    	// pullup
	P1OD = 0x00;    	// open drain
	P12DB = 0x00;   	// debounce : P23~20, P13~10
	P1   = 0x00;    	// port initial value
	
	P2IO = 0xFF;    	// direction
	P2PU = 0x00;    	// pullup
	P2OD = 0x00;    	// open drain
	P2   = 0x00;    	// port initial value
	
	P3IO = 0xBD;    	// direction
	P3PU = 0x00;    	// pullup
	P3OD = 0x00;    	// open drain
	P3   = 0x00;    	// port initial value
	
	// Set port functions
	P0FSR = 0x1B;   	// P0 selection
	P1FSRH = 0x6A;  	// P1 selection High
	P1FSRL = 0x55;  	// P1 selection Low
	P2FSR = 0x00;   	// P2 selection
	P3FSR = 0xC1;   	// P3 selection
}

