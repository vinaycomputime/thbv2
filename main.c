/*****************************************************************************
Automatic Balancing Valve
File: main.c
****************************************************************************/

// 															IMPORTANT   
// ************** Do NOT use the watchdog until fully tested ***************

//system Includes
#include	"AiP8F1016.h"
#include "main.h"
//#include "stdio.h"

//************************************************************************************
//											BALANCING VALVE MAIN PROGRAM
//************************************************************************************

/*****************************************************************************
Global Data Variables
*****************************************************************************/
xdata TIMERITEM TimerList[4];
xdata volatile BYTE second = 0;
//xdata WORD  dp11=0,dp12=0,dp13=0,dp14=0,dp15=0;
xdata BYTE minute = 0;
xdata BYTE minutes =0;
xdata BYTE hour = 0;
xdata WORD motortarget = 0;
xdata WORD currentmotorposition = 0;
xdata WORD maxcount = 0;
xdata WORD mtrcountfree = 0;
xdata WORD oldmtrcountfree = 0;
xdata WORD Fmaxcount;
xdata WORD Fvop;
xdata WORD Fcal;
xdata WORD Fontimes;
xdata WORD LKBP;
xdata BYTE open = FALSE;
xdata BYTE close = FALSE;
xdata volatile BYTE move = FALSE;
xdata BYTE flow = TRUE; // assume on the flow
xdata BYTE rturn = FALSE;
//xdata BYTE falling = FALSE;
//xdata BYTE maybefalling = FALSE;
xdata BYTE atmax = FALSE;
xdata BYTE atmin = FALSE;
xdata BYTE atmincount = 0;
xdata WORD olddt = 0;
xdata BYTE retry = 0;
xdata BYTE zeroed = FALSE;
xdata BYTE constate = BALANCING;
xdata BYTE subconstate = BALANCING;
xdata BYTE Balcycle	= 0;
xdata volatile BYTE testing = FALSE;

extern xdata int deltat;
extern xdata int sensor1;
extern xdata int sensor2;
extern xdata int flowtemp;
extern xdata int returntemp;
//AiP8F1016
extern xdata int motorCurrVAvgdbg;
extern xdata int valveInstallChk;
xdata BYTE flashRed = 0;
extern xdata BYTE temperr;
xdata BYTE idx = 0;
extern xdata int motorCurrVoltage[138];

//vinay
xdata int startCalib;
xdata char TempError;
xdata int testCount = 0;
xdata int testIncr = 0;
xdata int testCountProd = 0;
xdata BYTE testIncrProd = 0;
xdata BYTE testIncrCalib = 0;
xdata int testCountCalib = 0;
xdata BYTE CalibFlag = 0;
xdata BYTE powerOffFlag = 0;
xdata int testCountAdc = 0;
xdata int testIncrAdc = 0;
//AiP8F1016:
extern xdata int motorCurrVoltageAvg;
xdata BYTE balP;
extern xdata BYTE mtrRotate;
xdata BYTE testPCBA = TRUE;	//testing PCBA code
extern xdata BYTE phtSens;
//extern xdata BYTE temperr1;


BYTE ByteRead(int addr);
void ByteWrite(int addr, BYTE dat);

/*****************************************************************************
System Interrupts
*****************************************************************************/
void motor_isr (void) interrupt 0
{
	if(testing == TRUE)
	//if(testPCBA == TRUE)	
	{
	//AiP8F1016: Seems used to flash while motor movement indicating square wave sensor output	
	//if(P36) P36 = 0;
	//else P36=1;
		if(mtrRotate == FALSE && temperr == FALSE)// && temperr1 == FALSE)
		{
			P36 = 0;
			if(P33) P33 = 0;
			else P33=1;		
			//if(P12) P12 = 0;
			//else P12=1;
		}
		else if(phtSens == TRUE)
		{
			P36 = 0;
			if(P33) P33 = 0;
			else P33=1;				
		}
	}
	if(open)
	{
		mtrcountfree--;
		if((mtrcountfree-2 == motortarget) || (mtrcountfree < motortarget))
		{
			P24 = 0; // OK we are near our target
			P25 = 0; // so switch off the motor
			// will get picked up by the endstop detector, do nothing here
		}
	}
	if(close)
	{
		mtrcountfree++;
		if(mtrcountfree > 5000)	//20211016: case for NO VALVE 
		{
				P24=0;
				P25=0;			
		}			
		//AiP8F1016: Replace below check with Force >= 110N or based on Voltage reading(P06) only during Calibration
		//to find the hard close point
		else if(startCalib == 1)
		{
			//P36 = 1;	//test, coming here
			//if(Force>=110N or motorCurrVoltage >= 2100 ??)
			//if(motorCurrVoltage > 110)		//Max = 3.2mm
			//if(motorCurrVoltage > 300)		//Max = 4.01mm	
			//if(motorCurrVoltage > 400)		//Max = 3.224mm??		
			//if(motorCurrVoltage > 500)			//Max = 3.263mm??, for ADCDRH full max=3.920mm
			//if(motorCurrVoltage > 600)				//Max = 3.213mm

			//With Valve Install: 172,171,174,173,168,108,132  Without Valve Install: 139,138,137,133,135,132,141
			//if(motorCurrVoltageAvg > 140)		//raw adc => Voltage=0.65*adc -> 2700/4095=0.65
			//Force measurement from DQA, >20-160N, >15-98N, >13-74N  
			if((motorCurrVAvgdbg > 16) && (motorCurrVAvgdbg < 80))	 
			{
				//P36 = 1; //red led test
				//P33 = 0; //green led test
				P24=0;
				P25=0;
			}
		}
		else
		{
		if((mtrcountfree+2 == motortarget) || (mtrcountfree > motortarget))
		{
			P24=0; // OK near the target 
			P25=0; // so switch off the motor
			// will get picked up by the endstop detector, do nothing here
		}
		}
	}
}

// Callback Timer Interrupt
void timer_isr (void) interrupt 22
{
static BYTE idx;
//AiP8F1016: Update motorCurrVoltage maybe every 5secs, incr cnt until 2500 if it is 2ms timer
//After testing it appears to be 4ms timer,  
if((startCalib == 1)) //|| (powerOffFlag == 1))
{	
testCount++;
testCountAdc++;
	if(testCount>83){		//2000 will give 4*2000=8000ms=8sec, 20210916: 83 to read 3 times per sec for 56secs 
testCount = 0;
testIncr++;		//testing
}
	
if(testCountAdc>2500){		//To skip ADC record first 5 secs(Changing initial force) 
	testCountAdc = 0;		//since now stopping valve close based on adc read
	testIncrAdc++; 
}
}

//20211007: Generate 5sec delay after fully close during production test
if(startCalib == 2)
{	
testCountProd++;
	if(testCountProd>2500){ //1250){		//1250 will give 4*1250=5000ms=5sec
testCountProd = 0;
testIncrProd++;	// = 1;	//flag is set
}
}

if(CalibFlag == 1)
{	
testCountCalib++;
	if(testCountCalib>30000){ //1250){		//1250 will give 4*1250=5000ms=5sec
testCountCalib = 0;
testIncrCalib++;	// = 1;	//flag is set
}
}


for(idx=0;idx < 4;idx++)
	{
	if(TimerList[idx].callback != NULL) // is there a callback for this timer?
		{
		if(TimerList[idx].value <57601)  //Maximum timer period 120 seconds
			{ 
			TimerList[idx].value += 1;	//AiP8F1016: Every count generating 2.083ms
 			if(TimerList[idx].value >= TimerList[idx].msecs) // timer has timed out
				{
				TimerList[idx].timeout = TRUE;  // OK mark the timer as timed out, picked up when we poll the timer
				if(TimerList[idx].mode == TIMER_FOREVER) TimerList[idx].value = 0; // keep counting
				if(TimerList[idx].mode == TIMER_ONE_TIME) TimerList[idx].value = 57601;  // stop counting it
				}
			}
		}
	}
}

//AiP8F1016: THB V2 PRD: Eliminate VOP
#if 0
void findVOP(void)
{
BYTE i, j;	//Vinay
//vinay	
//startVoP = 1; 
TimerStart(DTIME,DeltaTime,28800,TIMER_FOREVER); // 1 minute timer 28800	
MoveToPosition(500); // get some water flowing before starting VOP
//sendstring("\n\rVOP\n\r\0");
sendstring("VOP Motor Pin:\0");
sendpin(mtrcountfree);	
move = FALSE;
		do
		{
		TimerPoll();
		readad();
		} while(move == FALSE);
	move = FALSE;
sendstring("\n\rFlow:\0");
sendtemp(flowtemp);
sendstring("Return:\0");
sendtemp(returntemp);
sendstring("Motor Pin:\0");
sendpin(mtrcountfree);	
Fcal = ReadCAL();
if(Fcal == 0xaa00)	MoveToPosition(maxcount-550); // start 0.6mm from absolute hard closed *****Changed******
//else MoveToPosition(ReadVOP()+100); // reconfirm the VOP
sendstring("\n\rStart VOP detection\n\r\0");
sendstring("Motor Pin \0");
sendpin(mtrcountfree);		
TimerReset(DTIME);
readad();
TimerStart(DTIME,DeltaTime,57599,TIMER_FOREVER); // 2 minute timer if low temp
//else 
//TimerStart(DTIME,DeltaTime,28800,TIMER_FOREVER); // 1 minute timer 28800	
move = FALSE;
for(j=0;j<5;j++)	//Vinay
{		
do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
move = FALSE;
}
//Vinay
/*	do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
move = FALSE;	
do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
move = FALSE;
	do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
move = FALSE;	
	do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
move = FALSE;
*/

if((flow == TRUE) || (rturn == TRUE))
{
	olddt = flowtemp;	
}
else
{
	do
	{
	TimerPoll();
	readad();
	} while(move == FALSE); //FALSE);
}
move = FALSE;
readad();
Fvop = maxcount-1500; // default in case we dont find the VOP, assume 1.5mm from maxcount
for(i=0;i<20;i++) // should be found within 2mm of absolute closed
	{	
		if(olddt > flowtemp) olddt = flowtemp; // use the lowest, if flowtemp increases use the olddt
		MoveToPosition(mtrcountfree-50); // move towards open by 0.05mm steps
		sendstring("\n\rFlow:\0");
		sendtemp(flowtemp);
		sendstring("Return:\0");
		sendtemp(returntemp);
		sendstring("Motor Pin:\0");
		sendpin(mtrcountfree);
		sendstring("Looking for:\0");
		sendtemp(olddt+50);
		do 
			{ 
			TimerPoll();
			readad();
			}while(move == FALSE);
		move = FALSE;
		if(flowtemp > olddt+50) // has flow increased by 0.5C?
		{
			sendstring("\n\rFound VOP \0");
			sendpin(mtrcountfree);
			sendstring("\n\rFlow:\0");
			sendtemp(flowtemp);
			sendstring("Return:\0");
			sendtemp(returntemp);
			sendstring("Motor Pin:\0");
			sendpin(mtrcountfree);
			Fvop = mtrcountfree-75; // OK we now have the VOP ****CHANGED for V0.53 add 75
			i = 20; // ok all done
			Fcal = 0xaa55; // update as calibrated
			//Fontimes=ReadOnTimes();
			EraseData();
			WriteMaxCount(Fmaxcount);
			WriteVOP(Fvop);
			WriteCAL(Fcal);
			WriteOnTimes(1);
			WriteFlash();
			Fmaxcount = ReadMaxCount();
			Fvop = ReadVOP();
			Fcal = ReadCAL();
			Fontimes = ReadOnTimes();
			motortarget = Fvop-1000;
			MoveToPosition(motortarget);
			//vinay
			//startVoP = 0;
		}
	}
	//Add error check led flash for Not found VOP
	//if(startVoP != 0)
	//{
	//	startVoP = 2;
	//}		
}
#endif

void sendrawtemp(WORD wdata)
{
	BYTE ln1, hn1, ln2, hn2;
	WORD x;
	x=wdata / 10;
	ln1 = wdata % 10;
	hn1 = x % 10;
	hn2 = x / 10;
	ln2 = hn2 % 10;
	hn2 = hn2 / 10;
	sendbyte((hn2|0x30));
	sendbyte((ln2|0x30));
	sendbyte('.');
	sendbyte((hn1|0x30));
	sendbyte((ln1|0x30));
}

void sendtemp(WORD wdata)
{
sendrawtemp(wdata);
sendbyte('C');
sendbyte(' ');	
}

void sendpin(WORD wdata)
{
sendrawpin(wdata);
sendbyte('m');
sendbyte('m');	
sendbyte(' ');
}

void sendrawpin(WORD wdata)
{
BYTE ln1, hn1, ln2, hn2;
WORD x;
x=wdata/10;
ln1 = wdata % 10;
hn1 = x % 10;
hn2 = x / 10;
ln2 = hn2 % 10;
hn2 = hn2 / 10;
sendbyte((hn2|0x30));
sendbyte('.');
sendbyte((ln2|0x30));
sendbyte((hn1|0x30));
sendbyte((ln1|0x30));		
}

void sendbyte(BYTE sdata)
{
	do
	{
	} while(((UARTST) & (0x80)) == 0);
	UARTDR = sdata;
}

void sendstring(BYTE *message)
{
	BYTE count = 0;
	do
	{
		sendbyte(message[count]);
		count++;
	} while(message[count] != NULL);
}

void DeltaTime(void)
{
	readad();	
//	flowtemp = 3500;
//	returntemp = 3100;
//	deltat = flowtemp - returntemp;
//	if((flowtemp < dp11) && (dp11 < dp12) && (dp12 < dp13) && (dp13 < dp14) && (dp14 < dp15)) falling = TRUE;
//	else falling = FALSE;
//	if((flowtemp < dp11) && (dp11 < dp12) && (dp12 < dp13)) maybefalling = TRUE;
//	else maybefalling = FALSE;
//	dp15 = dp14;
//	dp14 = dp13;
//	dp13 = dp12;
//	dp12 = dp11;
//	dp11 = flowtemp;
	move = TRUE;
}

/*****************************************************************************
 Timer Callback Routines
*****************************************************************************/
 
//Real Time Clock
void RealTime(void)
{
	// very simple RTC, just comes here every second
second++;
//AiP8F1016:
flashRed++;
if(flashRed>10)
flashRed=0;

if(testing == TRUE)
//if(testPCBA == TRUE)	
{
	PCBtest1();
}
//AiP8F1016:
if(P02==0)
//if(P13==0) // detect power is removed
{
	WORD i;
	for(i=0; i<1000;i++);
	//AiP8F1016:
	if(P02 == 0)
	//if(P13 == 0)
	{
	motor(OFF);  // just in case
	TimerReset(0); // kill the timers
	TimerReset(1);
	if(TempError == FALSE)
		TimerReset(2);
	TimerReset(3);
	if ((hour == 0) && (minutes == 0) && (second < 15))
	{
		//AiP8F1016:
		//P12 = 0; //Put the LED off
		//P36 = 0;
		P33 = 0;
		sendstring("\n\rPower OFF, ABA will NOT be re-calibrated at next power ON\n\r\0");
		reboot();
	}
	else if ((hour == 0) && (minutes < 2))
	{
			//EraseData(); // valve is already off, this is to ensure a re-calibration should be done
			//WriteCAL(0x00); // set as uncalibrated
			//WriteFlash();
			
			//added on 20211019:
			PageErase(MAXCOUNTADDR);
			//AiP8F1016: 20211011: clear Fcal to re-calib if power on for 15s-120s and power off 10s and power on again 
			WriteData(FCALADDR, 0x0000);
			//AiP8F1016:
			//P12 = 0; //Put the LED off
			//P36 = 0;
			P33 = 0;
		sendstring("\n\rPower OFF, Forcing an ABA re-calibration at next power ON\n\r\0");
			reboot();
	}
	powerisoff(); //  if not in the first two minutes will just shut down the ABA
	}
}
if(second == 60)
	{
	second = 0;
	minutes++;
	if(minute == 32) minute = 0; // stop 0 minutes during a roleover
	minute++;
	if(minutes == 60)
		{
		minutes = 0;
		hour++;
		} 
	}
}

// LED flasher
void FlashLed(void)
{
//AiP8F1016: P36 - LED1(RED), P33 - LED2(GREEEN)
if(startCalib==2)
{
	if(temperr == TRUE)
	{
		P33=0; //Green
		if (P36 == 0){	//Red led
			P36=1; } else { P36=0; }		
	}
	else
	{
	if(flashRed==3)
	{
		P33=0; //Green
		if (P36 == 0){	//Red led
			P36=1; 	
		} else {
			P36=0;
		}		
	}
	else if(flashRed==5)
	{
		flashRed=0;
		P33=0; //Green
		P36=0; //Red
	}
	}	
}
else
{
	P36=0;	//clear Red
	if (P33 == 0){ P33=1; }	//Green led
  else { P33=0;	}	
}
//if (P12 == 0) P12=1; // very simple LED flasher, if its on turn it off. If its off turn it on
//else P12=0; // happens dependant on the flash timer setting
}

// Motor end stop driver
void EndstopDriver(void)
{
	// detects when we are stalled. Called from a timer callback
	WORD i;
	if(open == TRUE || close == TRUE)
	{
		// motor should be opening or closing
		
		//AiP8F1016: Replace below check with Force >= 110N or based on Voltage reading(P06) only during Calibration
		//to find the hard close point
		/*if(startCalib)
		{
		//Already have this check in motor_isr so we may not needed this entire check	
			if(Force>=110N or motorCurrVoltage >= 2100 ??) 
			{
				P24=0;
				P25=0;
				for(i=0;i<10000;i++); // let the moror ccome to a halt IS THIS NEEDED AS WE HAD NO COUNT!!!
				P26 = 1;	//switch off the sensor
				P36 = 1;    //LED on
				currentmotorposition = mtrcountfree;
			}
		}
		else
		{*/
		if(oldmtrcountfree == mtrcountfree) // any counts since the last time?
		{
			if(mtrcountfree > 5000)	//20211016: case for NO VALVE 
			{
					P24 = 0;
					P25 = 0;
					TimerReset(3);
					for(i=0;i<10000;i++);
					P26 = 1;
					open = FALSE;
					close = FALSE;
					P33 = 1;
					currentmotorposition = mtrcountfree;
			}			
			//AiP8F1016: debug motor stop during adc read few times/sec
			else if(startCalib == 1)
			{
				//if(motorCurrVoltageAvg > 140)
				if((motorCurrVAvgdbg > 16) && (motorCurrVAvgdbg < 80))
				{	
					P24 = 0;
					P25 = 0;
					TimerReset(3);
					for(i=0;i<10000;i++);
					P26 = 1;
					open = FALSE;
					close = FALSE;
					P33 = 1;
					currentmotorposition = mtrcountfree;
				}
				//To handle No valve installation
				/*else if((motorCurrVoltageAvg > 100) && (motorCurrVoltageAvg < 140))
				{
					P24 = 0;
					P25 = 0;
					TimerReset(3);
					for(i=0;i<10000;i++);
					P26 = 1;
					open = FALSE;
					close = FALSE;
					currentmotorposition = mtrcountfree;
				}*/				
			}
			else
			{
			P24 = 0;		// Switch the motor off
			P25 = 0;		// needed for when we stall
			TimerReset(3); // OK we must be at the endstop
			if(TempError == FALSE)
				TimerReset(2);      // stop the LED flashing
			for(i=0;i<10000;i++); // let the moror ccome to a halt IS THIS NEEDED AS WE HAD NO COUNT!!!
			//AiP8F1016:
			//P16 = 1; // now switch off the sensor
			P26 = 1;
			open = FALSE; // OK no longer opening or closing
			close = FALSE;
			//AiP8F1016:
			//P12 = 1; // put LED ON (need to check if power off here !!)
			//P36 = 0;
			P33 = 1;	//Green LED
			currentmotorposition = mtrcountfree; // update where we are now
			}
		}
		else
		{
			oldmtrcountfree = mtrcountfree; // there was no stall detected, so update the old count
		}
		//}
	}
	else
	{
		// We are not opening or closing (should not happen, but safety first) so turn it all off
		TimerReset(3); // clear the two timers (endstop and led flash
		if(TempError == FALSE)
			TimerReset(2);
		//AiP8F1016:	
		//P12 = 1; // put LED ON (need to check if power off her !!)
		//P36 = 1;
		P33 = 1;	//Green LED
		currentmotorposition = mtrcountfree; // update where we are now
	}
}

void MotorCurrent(void)
{
	if(startCalib == 1)
	{	
		readMotorCurrentAD();
		sendpin(motorCurrVAvgdbg);
		sendpin(mtrcountfree);
	}
}

/*****************************************************************************
Main Program Start
*****************************************************************************/

void main(void)
{		
	SetPorts();  // setup the ports
	second = 0;
	minute = 0;
	hour = 0;
	TempError = FALSE;
	WDTinit(); // set the watch dog
	sendstring("\n\rTHB/ABA V2 Powered On V0.10\n\r\0");	//v0.02: Forcing re-calib, 0.03: stop based on adc value when below 100
	//v0.04: use mtrcountfree for No Valve case, v0.05: Added PageErase to correct FonTimes, removed red led lit 1 min
	//v0.06: Correct ADC reading for MotorCurrent similar to temp sensors, v0.07: ADC force check >20
	//v0.08: ADC force check >15, v0.09: ADC force check >13, v0.10: ADC force check >11
	//printf("New version ABA\n");
	//if(ReadOnTimes() != 0)
	if(ReadData(FONADDR) != 0)	
	{
	//sendstring("Motor Position: \0");
	/*sendpin(ReadMotorPosition());
	sendpin(ReadOnTimes());*/
	sendpin(ReadData(MTRADDR));
	sendpin(ReadData(FONADDR));	
	}
	//AiP8F1016: Need to ask IO pin for test mode??? It is P37 (test point T7)
	//20210902: Decided P34(T10) pin to use for test mode in new schematic
	//20210908: It seems P37(T7) pin used for PCBA test mode, hence use P37
	//P34 = 0;		//testing: simulate test mode
	//AiP8F1016: TEST PCBA code
	/*if(testPCBA == TRUE)
	PCBtest();
	do
	{
	TimerPoll();
	} while (testPCBA == TRUE);
	*/
	if(P37 == 0)
	//if(P03 == 0) 
	{
		testing = TRUE;
		PCBtest();
	}
	do
	{
	TimerPoll();
	} while (testing == TRUE);
	mtrcountfree = ReadMotorPosition();
	//AiP8F1016:
	mtrcountfree = ReadData(MTRADDR);
	BalanceSetup(); //setup the ABA
	TimerStart(DTIME,DeltaTime,28800,TIMER_FOREVER); // 2 minute timer // v0.541 changed to one minute (old 57599
	motor(OFF);
	readad();
	olddt = deltat;
	constate = BALANCING;
	subconstate = BALANCING;
	//sendstring("\n\rStart Balancing\n\r\0");
	LKBP = ReadLKBP();
	//AiP8F1016:
	LKBP = ReadData(LKBPADDR);

	Fvop = maxcount-400;		//0.4mm test //vinay: Debug fixed VOP
	if(LKBP == 0) LKBP = Fvop/2;
	//AiP8F1016: If not a good BP the pen by 0.4mm
	balP = ReadData(0x3c0a);
	if(balP == 0x01)
	{
		LKBP = LKBP+400; 	//Good BP: make sure no small flow hence close by 0.4mm	
	}
	else
	{
		LKBP = LKBP-400;	//Not a good BP: open by 0.4mm
		if(LKBP<800)	//20211015: change to 800, making sure it's near to fully open
		LKBP = 500;	
	}
	
	motortarget = LKBP;
	MoveToPosition(motortarget);
	sendstring("LKBP=\0");
	sendrawpin(LKBP);
	sendstring("\n\r\0");
	do 	
	{
		TimerPoll();
		//if(maxcount < 5150) // Always balance !!!!
		dobalance(); // OK lets keep the ABA to 7C delta T
	} while(1);
}

void dobalance(void)
{
WORD controlDT = 700;  // default 7C
BYTE diffDT;
readad();
//readMotorCurrentAD();	//AiP8F1016
TimerPoll();	
if(move)
	{
	Balcycle++;								// added for v0.541
	if(Balcycle  > 2)					// added for v0.541
	{													// added for v0.541
		Balcycle = 0;						// added for v0.541
	switch(constate)
		{
//		case BALSTART:
//			{
//			break;
//			}
		case BALANCING:
			{
			readad();
			//flowtemp = 3200; returntemp = 2700; deltat = flowtemp - returntemp;	//testing	
			TimerPoll();
			if(flowtemp < 4200) 
			{
				if(flowtemp >2800) controlDT = 300+35*((flowtemp-2800)/100);	//vinay: Achim,Manuel suggests for 3C minimum, change from 200 to 300
				else controlDT = 300;		//vinay: change from 200 to 300
			}
			//Vinay:- Achim suggestion: Have balancing at any temperature, comment below if block
			/*if(returntemp < 2600) 
				{
					motortarget = Fvop/2;
					atmax = TRUE;
					atmin = FALSE;
				}
			else
				{*/
//				if (flowtemp > 4000) controlDT = 700;
//			else if(flowtemp > 3900) controlDT = 650;
//			else if(flowtemp > 3800) controlDT = 600;
//			else if(flowtemp > 3700) controlDT = 550;
//			else if(flowtemp > 3600) controlDT = 500;
//			else if(flowtemp > 3500) controlDT = 450;
//			else if(flowtemp > 3400) controlDT = 400;
//			else if(flowtemp > 3300) controlDT = 350;
//			else if(flowtemp > 3200) controlDT = 300;
//			else if(flowtemp > 3100) controlDT = 250;
//			else controlDT = 200;

//			if(returntemp < 2600)
//			{
//				motortarget = 500;
//				atmax = TRUE;
//				atmin = FALSE;
//			}
//				else
//			{
				if(deltat > controlDT+150) //850)
					{
					motortarget = Fvop/2; //500; // Fvop-1000; 
					atmax = TRUE;									// OK have a high delta T so open to MPP to get maximum flow
					atmin = FALSE;
					}
				else if(deltat > controlDT+50) //750)
					{
					if(atmax || atmin) // we are < 8.5C but > 7.5C
						{
						atmax = FALSE; // ok came from min or max, so gentle
						atmin = FALSE;
						motortarget = Fvop-500;
						}
					else
						{
						if(olddt - 50 < deltat)
							{
							motortarget = mtrcountfree - 100; // Delta T is rising so open the ABA more
							if(motortarget < 500) // Fvop-1000) 
								{
								motortarget = Fvop/2; //500; // Fvop-1000; // maxcount/3; // Don't increase above the MPP
								atmax = TRUE;
								}
							}
						}
					}
				else if(deltat > controlDT+20) //720)
					{
					if(atmax || atmin) // we are < 7.5 but > 7.2
						{
						atmax = FALSE;
						atmin = FALSE;
						motortarget = Fvop-300;
						}
					else
						{
						if(olddt - 50 < deltat) // Are we rising
							{
							motortarget = mtrcountfree - 50;
							if(motortarget < 500) // Fvop-1000) 
								{
								motortarget = Fvop/2; //500; // Fvop-1000;
								atmax=TRUE;
								}
							}
						}
					}
				else if (deltat > controlDT-20) //680)
					{
					if(atmax || atmin) // we are < 7.2 but > 6.8, really do nothing
						{
						atmax = FALSE;
						atmin = FALSE;
						motortarget = Fvop-300;
						}
					}
				else if(deltat > controlDT-50) //650) // we are < 6.8 but > 6.5
					{
					if(atmax || atmin)
						{
						atmax = FALSE;
						atmin = FALSE;
						motortarget = Fvop-300;
						}
					else
						{
						if(olddt > deltat - 50) // Delta T is falling
							{
							motortarget = mtrcountfree + 50; // close the ABA a little
							if(motortarget > Fvop) 
								{
								motortarget = Fvop; // make sure not less than VOP
								atmin = TRUE;
								}
							}
						}
					}		
				else if(deltat > controlDT - 190) // Dont go 200 as could be 200 500) // we are < 6.5 but > 5
					{
					if(atmax || atmin)
						{
						atmax = FALSE;
						atmin = FALSE;
						motortarget = Fvop-100;
						}
					else
						{
						if(olddt > deltat -50 )
							{
							motortarget = mtrcountfree+50;
							if(motortarget > Fvop) 
								{
								motortarget = Fvop;
								atmin = TRUE;
								}
							}
						}
					}
				else
					{
	//				if(atmin)
	//					{
	//					if(falling) // changed from maybefalling
	//					{
	//						motortarget = Fvop-500;
	//						Fvop = Fvop-5;
	//					}
	//					atmin = FALSE;
						//Fvop = Fvop-100;
	//					}
	//				else
	//					{
						if (atmin == FALSE) 
						{
							minute = 0; // just start vop
							atmin = TRUE;
							motortarget = Fvop;
						}
							else if (minute == 10)
						{
							atmin = FALSE;
							motortarget = Fvop/2; //500;
						}
						else
						{
						atmin = TRUE;
						atmax = FALSE;
						motortarget = Fvop;
							// *************** TAKE THIS OUT ***************************************
	//					if(minute > 16) minute = 1;
	//					if((minute == 16) && (!falling))  // added on 07/11/2017
	//					{
	//						Fvop = Fvop+5; // test for an even lower VOP
	//						minute = 1;
	//					}
	//					motortarget = Fvop; // OK very low so VOP
						}
					}	
				//}		//Vinay
//			}
				if(motortarget > Fvop) motortarget = Fvop;
				if(motortarget < 500) motortarget = 500;
				MoveToPosition(motortarget);
				olddt = deltat;
				sendbyte(',');
				sendbyte(',');
				sendrawtemp(flowtemp);
				sendbyte(',');
				sendrawtemp(returntemp);
				sendbyte(',');
				sendrawtemp(flowtemp-returntemp);
				sendbyte(',');
				sendrawpin(mtrcountfree);
				LKBP = mtrcountfree;
				sendbyte(',');
				sendrawtemp(controlDT);
				//AiP8F1016
				//sendbyte(',');
				//sendrawtemp(motorCurrVoltage);
				//AiP8F1016: Detect good/bad balancing point
				if(controlDT > deltat)
					diffDT = controlDT-deltat;
				else
					diffDT = deltat-controlDT;

				if(diffDT>20)
				WriteData(0x3c0a, 0x00);	//Not a good BP
				else
				WriteData(0x3c0a, 0x01);	//Good BP
				
//				if(falling) sendstring(" Falling\n\r\0");
//				else if(maybefalling) sendstring(" Maybe Falling\n\r\0");
				sendstring("\n\r\0");
//			  if(mtrcountfree > Fvop-20) atmincount++;
//				if(atmincount > 3) // stop being at VOP for too long
//				{
//					atmincount = 0;
//					motortarget = Fvop/2;
//					MoveToPosition(motortarget);
//				}
					
		break;	
			}
		}
//		case BALBELOW:
//			{
//			
//			break;
//			}
//		case BALABOVE:
//			{
//			
//			break;
			}
	move = FALSE; // OK reset move and wait two minutes
	}
}
void powerisoff(void)
{
			// Sensed the power is off	
			motor(OFF); // ensure the motor is off
			sendstring("\n\rPower OFF\0");
			P36 = 0;	//AiP8F1016						
			//20211013: DO NOT valve close when in production test after power off
			if(P34 == 0)
			//if(testPCBA == TRUE)	//Simulate T10 connect to GND
			{
				P33 = 0;
				reboot();
				return;
			}

			if(zeroed)
			{
				Fcal = ReadCAL();
				//AiP8F1016:
				Fcal = ReadData(FCALADDR);
				
				if((Fcal == 0xaa55) || (Fcal == 0xaa00))
				{
					//AiP8F1016: test Fmaxcount, fully close with force 110N, comment led flash
					//motortarget= maxcount - 110;  // come back to 0.5mm, 20210601: 0.1mm, AiP8F1016: may remove 110 since close with 110N					
					motortarget= maxcount; //Fmaxcount;
				//TimerStart(LED,FlashLed,640,TIMER_FOREVER);  // do a special valve close
				open = FALSE;
				close = TRUE;
				//AiP8F1016:				
				//P16 = 0; // put sensor on
				P26 = 0;				
				P25 = 0; // set to close
				P24 = 1; //// switch the motor on
				TimerStart(ENDSTOP,EndstopDriver,50,TIMER_FOREVER); // check for stall every 104ms count
				powerOffFlag = 1;
				do {
				TimerPoll();
				//AiP8F1016:
				//20211011: may need to test motor current read when power off during close				
					/*if(testIncr > 1){
						testIncr = 0;
						readMotorCurrentAD();
					}*/
				} while(close || open);

				//AiP8F1016: //20211011:
				//for(idx=0;idx<138;idx++)
				//sendpin(motorCurrVoltage[idx]); 

				//AiP8F1016: commented for testing
				/*EraseData();
				WriteMaxCount(Fmaxcount);
				WriteLKBP(LKBP);
				WriteVOP(Fvop);
				WriteCAL(Fcal);
			  WriteOnTimes(Fontimes);*/
			  //added on 20211019:
			  PageErase(MAXCOUNTADDR);
				//AiP8F1016:
				WriteData(MAXCOUNTADDR, Fmaxcount);
				WriteData(LKBPADDR, LKBP);
				WriteData(FCALADDR, Fcal);		
				WriteData(FONADDR, Fontimes);				
				
				//AiP8F1016: commented for testing
				if((Fontimes % 10) == 0) mtrcountfree = 0; // force every ten on times
				//WriteMotorPosition(mtrcountfree);
				//WriteFlash();
				
				//AiP8F1016:
				WriteData(MTRADDR, mtrcountfree);				

				//AiP8F1016:
				//P12 = 0; //Put the LED off
				//P36 = 0;	
				P33 = 0;	//Green LED
				P36 = 0;	//Red LED
				}
				else
				{
				if(maxcount > 5149) // OK we have no valve fitted
					{
					// MoveToPosition(50);
					MoveToPosition(7000); // stop leaving the ABA full open
					//LedFlash(50,2);	//AiP8F1016:
					//AiP8F1016:
					//P12 = 0;
					//P36 = 0;
					P33 = 0;	
					P36 = 0;	
					}
				else
					{
					MoveToPosition(7000); //MoveToPosition(7000);		//For easy valve installation after zero ref move to 0
					//AiP8F1016:
					//P12=0;
					//P36=0;
					P33 = 0;
					P36 = 0;
					}
				}
			}
			else // not yet zeroed
			{
				MoveToPosition(7000); //MoveToPosition(0);	//For easy valve installation test //MoveToPosition(7000);
				//AiP8F1016:
				//P12 = 0;
				//P36 = 0;
				P33 = 0;
				P36 = 0;
			}
			sendstring("Motor Pin:\0");
			if(mtrcountfree < 5300)
			{
			sendpin(mtrcountfree);
			}
			else
			{
				sendstring("Hard close\0");
			}
		reboot();
}



void SetPorts(void)
{
		// setup the timer BEFORE setting interrupts etc.
		BYTE idx;
		for(idx=0;idx<MAX_TIMERS;idx++)
		{
		TimerList[idx].callback = NULL;
		TimerList[idx].msecs = 0;
		TimerList[idx].value = 0;
		TimerList[idx].mode = 0;
		TimerList[idx].timeout = FALSE;
		}

		//20210702: AiP8F1016 mcu IO settings
	P0 = 0x00; 
	P0IO = 0x00;	//AC detect input pin hence I/O config P02 is 0
	P0PU = 0x04;	//AC detect pull up
	P0OD = 0x00;
	P0FSR = 0xB0;//0x58; //0xB0;	//AN3, AN4, AC detect is an input pin,
								//ADCCRL=0xC6; (AN6 for Motor CURRENT, analog reading->analog value->Vo=Vcc(R/(R+FSR))->FSR->Conductance->Force(N))
	P0 = 0x00;

	P1 = 0x00; 
	P1IO = 0x00;
	P1PU = 0x00;
	P1OD = 0x00;
	P1FSRL = 0x00;
	P1FSRH = 0x00;	
	P1 = 0x00;
		
	P2 = 0x40;	//P26 sensor 
	P2IO = 0x70;	//P24,P25,P26 -> mot1, mot2, sensor
	P2PU = 0x00;
	P2OD = 0x40;
	P2FSR = 0x00;	
	P2 = 0x40;

	P3 = 0x00;
	P3IO = 0x49;	//LED1,LED2,TXD
	//20211013: use P34(T10) to skip super cap discharge to avoid fully close after power off
	P3PU = 0x90; //0x80; //0x10; //0x80;	//Test mode P37(T7), Changed to T10 for test mode P34, P37(T7) for PCBA test
	P3OD = 0x00;
	P3FSR = 0x01;	//Select TXD	
	UARTCR1 = 0x06;		//No parity, 8 bit
	//OSCCR: 0x08(1MHz)->12(4800),0x10(2MHz)->12(9600)
	UARTBD = 12;			//9600 baud rate, 3 for 115200
	UARTCR2 = 0x0A;		//TX and UART Enable, 0x0E for RX 
	P3 = 0x00;
	
  //AiP8F1016: Interrupts
	IE = 0x81;
	EIPOL1 = 0x02;	//Falling Edge interrupt
	BITCR = 0x4b;	// BCLK = fx/128, BCLK * 16, Timer: 2MHz, 1/2=0.5uS 0r 0.0005 -> 2msec???? 
	IE3 = 0x10; // enable BIT timer interrupt 
	IE = 0x81;	//Enable EINT10 for motor sensor		

//vinay: testing
	// internal RC clock (8.000000MHz) 0x20
	//0x20 - 8MHz, 0x10 - 2MHz, 0x08 - 1MHz, 0x28 - 16MHz
	OSCCR = 0x08;  //0x10 for 2MHz try testing 	// Set Int. OSC			//vinay
	SCCR = 0x00;    	// Use Int. OSC

	/**** MC96F8208S
	P0 = 0x00; 
	P0IO = 0x00; // changed from 0x08
	P0PU = 0x08; // changed from 0x07
	P0OD = 0x00;
	P0FSR = 0x00;
	P0 = 0x00;
	P1 = 0x40; 
	P1IO = 0x46;
	P1PU = 0x08; // AC detect needs pull up
	P1OD = 0x40;	  
	P1FSRL = 0x00;
	P1FSRH = 0x0A;
	P1 = 0x40;
	P2 = 0x00; 
	P2IO = 0x30;
	P2PU = 0x00;
	P2OD = 0x00;
	P2FSR = 0x00;
	P2 = 0x00;

	P3 = 0x00; 
	P3IO = 0x01; // changed from 0x00
	P3PU = 0x00;
	P3OD = 0x00;
	P3FSR = 0x01;
	UARTCR1 = 0x06;
	UARTBD = 12;
	UARTCR2 = 0x0A;
	P3 = 0x00;
   // Interrupts
	IE = 0x81;
	EIPOL1 = 0x02;

	// Basic free running interrupt timer
	BITCR = 0x4b; //0x4b; //0x4c; //0x4B;  // clear the timer 30mS
	IE3 = 0x10; // enable bit 
	IE = 0x81;		// Enable interrupts and enable INT10 (motor pulse
	*/
}

void BalanceSetup(void)
{
		retry =0;
	  disablerst();
	// ******** check which is the flow sensor ***************************
		TimerStart(RTC,RealTime,480,TIMER_FOREVER); // 1 second timer
		second = 0;
		//AiP8F1016:
		//P12=1; // put led on
		//P36=1;
		P33=1;
		do
		{
			TimerPoll();
		}while(second !=5);
		//TimerStart(LED,FlashLed,50,TIMER_FOREVER); // Fast LED flash
		//AiP8F1016: 1tick/sec->flash/2sec->ON-1s,OFF-1s
		TimerStart(LED,FlashLed,480,TIMER_FOREVER);
	  do 
			{
			TimerPoll();  
			} while(minute != 2); // Let the supercap charge first
   // move to position 0 and get the reference

		//startCalib = 1;		//AiP8F1016:	Enable when Calibration started after Zero ref completion

		//AiP8F1016: Enabled Fon based zero ref	
		//if((ReadOnTimes() == 0) || (mtrcountfree == 0))
		//do{	//20211023: TEST in loop	
		if((ReadData(FONADDR) == 0) || (mtrcountfree == 0))	
		{
		mtrcountfree = 7000; //make sure we can hit the endstop assume well outside valve limits		
		currentmotorposition = mtrcountfree;
		oldmtrcountfree = mtrcountfree; // get ready
		MoveToPosition(0); // Find the zero reference the only time we do this
		motortarget = 100; // makes sure if switched off we are not now trying to find zero
		//LedFlash(50,2);	//AiP8F1016: removed
		mtrcountfree = 0; // our zero reference, now we are referenced
		zeroed = TRUE;
		currentmotorposition = 0;
		sendstring("Motor Pin:\0");
		sendpin(mtrcountfree);
		sendstring("\n\rABA Zero Pin calibrated\0");
		}
		else
		{
			//mtrcountfree = ReadMotorPosition();
			mtrcountfree = ReadData(MTRADDR);
			currentmotorposition = mtrcountfree;
			sendstring("ABA is at \0");
			sendpin(mtrcountfree);
		}
		TimerPoll();
		readad();
		sendstring("\n\rSensors:S1=\0");
		sendtemp(sensor1);
		sendstring("S2=\0");
		sendtemp(sensor2);
		//Fontimes = ReadOnTimes();
		//Fcal = ReadCAL();
		//AiP8F1016:
		Fontimes = ReadData(FONADDR);		
		Fcal = ReadData(FCALADDR);
			
//		if(Fontimes > 300) 
//			{
//				Fcal = 0; // do re-cal when 300 on/offs
//			}
		if(Fcal == 0xaa55) // are we fully calibrated?
		{	
			sendstring("\n\rABA is already calibrated, VOP:\0");
			maxcount = ReadMaxCount(); // get the max count
			Fmaxcount = maxcount;
			Fvop = ReadVOP();
			sendpin(Fvop);
			Fontimes = ReadOnTimes();
			Fontimes = Fontimes+1;
			LKBP = ReadLKBP();
			mtrcountfree = ReadMotorPosition();
			currentmotorposition = mtrcountfree;
			zeroed = TRUE;
			EraseData();
			WriteMaxCount(Fmaxcount);
			WriteVOP(Fvop);
			WriteCAL(Fcal);
			WriteOnTimes(Fontimes);
			WriteLKBP(LKBP);
			WriteFlash();
			Fmaxcount = ReadMaxCount();
			Fvop = ReadVOP();
			Fcal = ReadCAL();
			Fontimes = ReadOnTimes();
//			sendstring("\n\r Max Count, On Times, VOP, LKBP \n\r\0");
//			sendrawpin(Fmaxcount);
//			sendstring("  \0");
//			sendrawpin(Fontimes);
//			sendstring("  \0");	
//			sendrawpin(Fvop);
			sendstring("LKBP \0");
			sendrawpin(LKBP);
			MoveToPosition(Fvop-500); // *********BUG***** should be (Fvop-500)
			//V0552.6	
			startCalib = 0;				
		}
		else if(Fcal == 0xaa00) // is the maxcount done but not the VOP?
		{
			sendstring("\n\rABA has max pin calibrated \0");
			maxcount = ReadMaxCount(); // get the max count
			//AiP8F1016:
			maxcount = ReadData(MAXCOUNTADDR);
			Fcal = ReadData(FCALADDR);
			//AiP8F1016: since no VOP hence no 0xaa55, have Fon,motorpos flash here
			Fontimes = ReadData(FONADDR);	
			Fontimes = Fontimes+1;
			WriteData(FONADDR, Fontimes);
			
			mtrcountfree = ReadData(MTRADDR);
			currentmotorposition = mtrcountfree;
			zeroed = TRUE;

			Fmaxcount = maxcount;
			sendpin(maxcount);			
			//findVOP();	// find the VOP, 20210601: vinay: Disable VOP finding
			//V0552.6	
			startCalib = 0;							
		}
		else
		{
			//AiP8F1016:
			startCalib = 1;		
			//TimerStart(MTRCURR,MotorCurrent,480,TIMER_FOREVER);
			
			sendstring("\n\rABA is not calibrated\0");
		// Totally uncalibrated need to find the max count & VOP
		MoveToPosition(7000); // Find the max count
		if(mtrcountfree < 3000)
		{
			MoveToPosition(7000); // if less than 3000 try again
			retry++;
		}
			if(mtrcountfree < 3000)
			{
				MoveToPosition(7000);  // if less than 3000 try again
				retry++;
			}
			sendstring("\n\rABA valve max pin calibrated\0");
			TimerPoll();
			maxcount = mtrcountfree; // now at fully closed hard stop
			Fmaxcount = maxcount;
			sendpin(maxcount);
			//AiP8F1016: print motor current voltage array
			for(idx=0;idx<138;idx++)
			sendpin(motorCurrVoltage[idx]);	

			//AiP8F1016:
			//TimerReset(MTRCURR);
			if((maxcount < 4900) && (maxcount > 500))	//No valve installed then max count found approx 5200
			//if(valveInstallChk >= 10)	
			//AiP8F1016:	maxcount found is 5203, 
			//To test balancing without valve connected change maxcount check to 5200
			//20210902: check valveInstallChk count for 3 to see if adc value of motor current exceeded 170 atleast 3 times
			//					to decide valve installation
			//20210910: Since now reading 3 times per second hence can increase the range of valveInstallChk from 3 to 10
				{
				Fcal = 0xaa00; // OK now we have the max count
				valveInstallChk = 0;	
				//AiP8F1016: comment below lines	
				/*EraseData(); // need to update the on / off times 
				WriteMaxCount(Fmaxcount);
				WriteCAL(Fcal); // update to max count is calibrated
				WriteOnTimes(0); // zero on times
				WriteFlash();*/	
				//added on 20211019:
				PageErase(MAXCOUNTADDR);
				//AiP8F1016:	
				WriteData(MAXCOUNTADDR, Fmaxcount);
				WriteData(FCALADDR, Fcal);	
				WriteData(FONADDR, 0);	// zero on times

				//CalibFlag = 1;
				
				TimerPoll();
				//Not needed to indicate
				/*sendpin(testIncrCalib);
				do{
					TimerPoll();
					P33 = 0;
					P36 = 1;
				}while(testIncrCalib==0);
				sendpin(testIncrCalib);
				P33 = 1;
				P36 = 0;*/

//				sendstring("\n\rValve is connected \0");
					//findVOP();			// find the VOP, 20210525: vinay: Disable VOP finding
				//V0552.6	
				startCalib = 0;	
				}
			else
			{
				//V0552.6	
				startCalib = 2;
				valveInstallChk = 0;
				sendstring("\n\rNo valve is connected to the ABA/0");
				//20211007: 5sec delay after fully close during production test
				sendpin(testIncrProd);
				do{
					TimerPoll();
				}while(testIncrProd==0);
				sendpin(testIncrProd);
				
				//20211007: Keep fully open for easy installation after factory test
				MoveToPosition(0);	//MoveToPosition(2200);				
				//AiP8F1016: test sensor error 
				/*if(P34 == 0)	//if(testing == TRUE)	//volatile
				{
				P36 = 0;
				P33 = 1;
				readad();
				if((sensor1 > 3500) || (sensor2 > 3500) || (sensor1 < 1800) || (sensor2 < 1800))
				{
					LedFlash(250,1);
					LedFlash(130,1);
				}
				}
				else
				{*/
				//sensor1 = 900;	//testing
				/*if((sensor1 <1000) || (sensor1 > 3600) || (sensor2 < 1000) || (sensor2 > 3600)) 
				{
					if(temperr == FALSE)	
					{
					TimerStart(LED,FlashLed,100,TIMER_FOREVER);
					temperr = TRUE;
					}
				}*/
				TimerStart(LED,FlashLed,50,TIMER_FOREVER);	//For 120: 120*2 = approx. 250 means 0.25 -> 0.25ON/OFf->0.5flash->2Hz
				do {
					TimerPoll();
				}
				while(P02!=0);	//AiP8F1016:
				//while(P13!=0);
				//}
				
//				sendstring("\n\r No valve is connected to the ABA \0");
				//commented below in V0552.6
				//LedFlash(50,2);
				//MoveToPosition(2200);
				//readad();
				//if((sensor1 > 3500) || (sensor2 > 3500) || (sensor1 < 1800) || (sensor2 < 1800))
				//{
				//	LedFlash(250,1);
				//	LedFlash(130,1);
				//}
			}
		}
		//TEST in loop
	//	mtrcountfree = 0;
	//	PageErase(MAXCOUNTADDR);		
	//}while(1);
}

void LedFlash(BYTE count,BYTE rate) // Used for FG test
{
	BYTE i;
	unsigned int j;
	if(rate == 1)
	{
		for(i=0;i!=count;i++)
		{
			//AiP8F1016:
			//P12 = 0;
			P36 = 0;		//Red
			//P33 = 0;	//Green
			for(j=0;j!=5000;j++);
			TimerPoll();
			//P12 = 1;
			P36 = 1;
			//P33 = 1;
			for(j=0;j!=5000;j++);
			TimerPoll();
		}
	}
	if(rate == 2)
	{
		for(i=0;i!=count;i++)
		{
			//AiP8F1016:
			//P12 = 0;
			P36 = 0;
			//P33 = 0;
			for(j=0;j!=7500;j++);
			TimerPoll();
			//P12 = 1;
			P36 = 1;
			//P33 = 1;
			for(j=0;j!=7500;j++);
			TimerPoll();
		}
	}
	if(rate == 3)
	{
			for(i=0;i!=count;i++)
			{
			//AiP8F1016:
			//P12 = 0;
			P36 = 0;
			//P33 = 0;
			for(j=0;j!=30000;j++);
			//P12 = 1;
			P36 = 1;
			//P33 = 1;
			for(j=0;j!=30000;j++);
			}
	}
}
