
 /* 
 Automatic Balancing Valve
 File: flash.c
 */
 
#include	"AiP8F1016.h"
#include "main.h"
 
 //************************************************************************************
//											BALANCING VALVE FLASH READ & WRITE
//************************************************************************************

 
 
 xdata volatile BYTE eeval1; // return value
 xdata volatile BYTE eeval2;

//AiP8F1016:
#define CBYTE ((unsigned char volatile code  *) 0)
 
// Mostly an assembly file as it takes less space and is faster
 
 // ********************NEED TO TEST AS NOT YET FULLY TESTED*************************************
void reboot(void)
{
	WORD i;
	do
	{
		do
		{
		// need to add the check for ABA is really off with a 10 minute check of the flow temperature
		TimerPoll();
		//AiP8F1016:
		}while(P02 == 0);
		//}while(P13 == 0); // do forever power is off (just for now)
		
	for(i=0;i<10000;i++);
	//AiP8F1016:	
	}while(P02 == 0);
	//}while(P13 == 0);
	redoit();
}
void redoit(void)
{
	#pragma asm
	JMP 0
	#pragma endasm
}

void disablerst(void)
{
#pragma asm
	MOV RSTFR,#0
	MOV LVICR,#0
	MOV LVRCR,#0x01
	NOP
	MOV LVRCR,#0x01
	NOP
	MOV LVRCR,#0x01
#pragma endasm
}

//AiP8F1016: Erase 1K from addr
void PageErase(int addr)
{
	addr = addr & 0xfc00;
	
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	FSADRH = 0x00;
	FSADRM = (BYTE)(addr >> 8);
	FSADRL = (BYTE)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (BYTE)(addr >> 8) 
		&& FSADRL == (BYTE)(addr & 0xff))
	{
		FMCR = 0x02;
		_nop_();
		_nop_();
		_nop_();
	}
}

//AiP8F1016: uint16 -> int, uint8 -> BYTE
void ByteWrite(int addr, BYTE dat)
{
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	*((BYTE xdata *)0x8000) = dat;
	
	FSADRH = 0x00;
	FSADRM = (BYTE)(addr >> 8);
	FSADRL = (BYTE)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (BYTE)(addr >> 8) 
		&& FSADRL == (BYTE)(addr & 0xff))
	{
		FMCR = 0x03;
		_nop_();
		_nop_();
		_nop_();
	}
}

BYTE ByteRead(int addr)
{
	return CBYTE[addr];
}

int ReadData(int addr)
{
	BYTE data8;
	int data16;
	data8 = ByteRead(addr+1);	//higher byte
	data16 = data8;
	data8 = ByteRead(addr);		//lower byte
	data16 = (data16<<8) | data8;
	return data16;
}

void WriteData(int addr, int dataval)
{
		ByteWrite(addr, (dataval&0x00ff));	
		ByteWrite(addr+1, ((dataval&0xff00)>>8));		
}	
	
//AiP8F1016: For 16KB flash -> address 3FA0 (change 1F to 3F)
void EraseData(void)
	{
	#pragma asm
	MOV FMCR,#0x01
 	NOP
	NOP
 	NOP
	MOV A,#0
	MOV R0,#32
	MOV DPH,#0x80
	MOV DPL,#0
	Pgbufclr: MOVX @DPTR,A
	INC DPTR
	DJNZ R0,Pgbufclr
	MOV FSADRH,#0x00
  MOV FSADRM,#0x3F
  MOV FSADRL,#0xA0
  MOV FIDR,#0xA5
  MOV FMCR,#0x02
  NOP
  NOP
  NOP
	#pragma endasm
	}

void WriteFlash()
	{
	#pragma asm
	MOV FSADRH,#0x00
  MOV FSADRM,#0x3F
  MOV FSADRL,#0xA0 
  MOV FIDR,#0xA5
  MOV FMCR,#0x03
  NOP
  NOP
  NOP	
	#pragma endasm
	}
	
	
void WriteMaxCount(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x00
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x01
  MOVX @DPTR,A
 #pragma endasm
 	}
 
void WriteVOP(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x02
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x03
  MOVX @DPTR,A
 #pragma endasm
 	}

void WriteCAL(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x06
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x07
  MOVX @DPTR,A
 #pragma endasm
 	}
	
void WriteOnTimes(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x08
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x09
  MOVX @DPTR,A
 #pragma endasm
 	}
	
void WriteMotorPosition(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x0A
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x0B
  MOVX @DPTR,A
 #pragma endasm
 	}
	
	
void WriteLKBP(WORD eedata)
 	{
 	#pragma asm	
	MOV A,R6
 	MOV DPH,#0x80
 	MOV DPL,#0x0C
	MOVX @DPTR,A
  MOV A,R7
  MOV DPH,#0x80
  MOV DPL,#0x0D
  MOVX @DPTR,A
 #pragma endasm
 	}
	
	
WORD ReadMaxCount(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xA0   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xA1    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}

WORD ReadVOP(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xA2   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xA3    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}

WORD ReadCAL(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xA6   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xA7    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}

WORD ReadOnTimes(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xA8   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xA9    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}
WORD ReadMotorPosition(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xAA   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xAB    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}
	
WORD ReadLKBP(void)
{	
 	#pragma asm	
				MOV A,#0x00
 				MOV DPH,#0x3F        
 				MOV DPL,#0xAC   
 				MOVC A,@A+DPTR
 				MOV DPTR, #eeval1
 			  MOVX @DPTR,A
 				MOV A,#0x00
 				MOV DPH,#0x3F      
 				MOV DPL,#0xAD    
 				MOVC A,@A+DPTR
 				MOV DPTR,#eeval2
 				MOVX @DPTR,A
 	#pragma endasm
 	return ((eeval1 <<8 ) + eeval2);
 	}
	
	
	
