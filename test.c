/* 
Automatic Balancing Valve
File: test.c
*/

//system Includes
#include	"AiP8F1016.h"
#include "main.h"

extern xdata BYTE second;
extern xdata BYTE open;
extern xdata int sensor1;
extern xdata int sensor2;
xdata BYTE temperr = 0;//= FALSE;
//xdata BYTE temperr1 = FALSE;
xdata BYTE mtrRotate = FALSE;
xdata BYTE phtSens = FALSE;

extern xdata int startCalib;

void ClearWDT(void)
{
 WDTCR |= 0x20;   // Clear the Watch-dog 
}

void WDTinit(void)  // ***********Get ready to use this when the software is fully debugged**********
{
 // Set the Watch-dog timer
 WDTDR = 0xF0;     
 WDTCR = 0xC2;     
 ClearWDT();
}

void PCBTest1(void)
{
	if(second == 4) 
	{
		TimerReset(LED);
		//AiP8F1016:
		//P12 = 1;
		//AiP8F1016: As per test spec, when MCU in active mode for 5s turn on both red and green led (visibly looks orange)
		P33 = 1;
		P36 = 1;
	}
	//if(second >9)	//AiP8F1016: Bug
	if(second == 9)	
	{
		readad();
		//if((sensor1 <1000) || (sensor1 > 3600) || (sensor2 < 1000) || (sensor2 > 3600))
		//AiP8F1016: New test spec requirement - 10K+/-200 resistor which is around 25C
		//20210923: Both Temp1, Temp2 detected 10K resistor(25degc range),red LED fast flash
		//			No 10K resistor detection, green LED fast flash
		//sensor1 = 2600;sensor2 = 2600;
		//if((sensor1 <= 2400) || (sensor1 >= 2600) || (sensor2 <= 2400) || (sensor2 >= 2600))
		if((sensor1 > 2400) && (sensor1 < 2600) && (sensor2 > 2400) && (sensor2 < 2600))
		{
			if(temperr == FALSE)	
			{
				TimerStart(LED,FlashLed,50,TIMER_FOREVER); 
				temperr = TRUE;
				startCalib = 2;	//AiP8F1016: ???		// for red LED flash
			}
		}
		else
		{		
			TimerStart(LED,FlashLed,50,TIMER_FOREVER);	//green led flash	
		}	
		//AiP8F1016: New test spec requirement, 10K+/-200 Ohm connect, sensor1 - red LED flash, sensor2 - green LED flash
		/*if((sensor1 > 2400) && (sensor1 < 2600))
		{
				TimerStart(LED,FlashLed,50,TIMER_FOREVER);		//red LED flash
				temperr = TRUE;
				startCalib = 2;
		}
		else if((sensor2 > 2400) && (sensor2 < 2600))
		{
			startCalib = 0;
			TimerStart(LED,FlashLed,50,TIMER_FOREVER);			//green LED flash
		}*/
	}
	//AiP8F1016: As per test spec, rotate clockwise 5s and anti-clockwise 5s
		//if(second >13) second = 10;
	//if(second > 27) second = 15;
	//if(second > 42) second = 15;
	//if(second > 37) second = 15;	
	//if(second > 25)	TimerReset(LED); 
	if(second > 32) second = 15;
	if(second == 15)
	{	
			//AiP8F1016:	
			//Slow red flash to indicate Photo_Sensor_Ctl is set to LOW
			TimerReset(LED);		//?? may required due flash in temperature test			
			P26 = 0;
			//P16 = 0; // put sensor on
			P24 = 1; // P25 = 0;
			P25 = 1; // P24 = 1; // switch the motor on
			mtrRotate = FALSE;
			phtSens = FALSE;
	}
	if(second == 16)
	{	
			//AiP8F1016:			
			P26 = 0;
			//P16 = 0; // put sensor on
			P24 = 0; // P25 = 0;
			P25 = 1; // P24 = 1; // switch the motor on
			//AiP8F1016: There is LED flash in motor_isr, hence need to handle	
			// Is this required?? because there is slow red led flash
			mtrRotate = TRUE;
			P33 = 0;
			P36 = 1;
	}
	//if(second == 12)
	if(second == 21)	//AiP8F1016:	
	{
			//AiP8F1016:			
			P26 = 0;
			//P16 = 0; // put sensor on
			P24 = 1; // P25 = 0;
			P25 = 1; // P24 = 1; // switch the motor on
			mtrRotate = FALSE;
	}
	//if(second == 13)
	if(second == 22)	//AiP8F1016:
	{	
			//AiP8F1016:			
			P26 = 0;
			//P16 = 0; // put sensor on
			P25 = 0; // P25 = 0;
			P24 = 1; // P24 = 1; // switch the motor on
			//AiP8F1016:
			mtrRotate = TRUE;
			P33 = 1;
			P36 = 0;			
	}
	//if(second > 32) second = 27;
	//PhotoSensor Circuit Testing: When you set �MOT1� and �MOT2� at high or low at the same time normally, 
	//					   the motor should not rotate, then there is no square wave at �SENSOR_OUT�. 
	//					   But if you change the  �MOT1� and �MOT2� to high or low at the same time 
	//					   when the motor is rotating, the motor can�t stop immediately but rotate for
	//					   short time because of inertia force, so there is still some square waves at  �SENSOR_OUT�.
 
	if(second == 27)
	{
		P26 = 0;		
		P24 = 1;
		P25 = 1;
		TimerStart(LED,FlashLed,240,TIMER_FOREVER); 
		temperr = TRUE;
		startCalib = 2;
		phtSens = TRUE;
	}	
	if(second == 30)
	{
		P26 = 0;		
		P24 = 0;
		P25 = 1;
	}
	if(second == 31)
	{
		P26 = 0;		
		P24 = 1;
		P25 = 1;
	}
	/*if(second == 30)
	{
		P26 = 0;		
		P24 = 1;
		P25 = 0;
	}	
	if(second == 31)
	{
		P26 = 0;		
		P24 = 1;
		P25 = 1;
	}*/	
	/*if(second == 37)
	{
		P26 = 0;
		P25 = 0;
		P24 = 0;
	}*/
TimerPoll();
}

void PCBtest(void)
{
	second = 0;
	TimerStart(LED,FlashLed,240,TIMER_FOREVER); // slow Test LED flash
	TimerStart(RTC,RealTime,480,TIMER_FOREVER); // 1 second timer
	open = TRUE;
}
