#include "flash.h"

/******************************************
函数名称：PageErase
函数作用：擦除Flash一页(1k)
返回值：无
参数： 无符号16位整数 addr -- 待擦除的页首地址

注：
1、如果给入的参数不是页首地址，则擦除该地址所在页的内容。
   比如给入0x3cdf地址，则实际擦除的是0x3c00地址开始的1k内容。
*******************************************/
void PageErase(uint16 addr)
{
	addr = addr & 0xfc00;
	
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	FSADRH = 0x00;
	FSADRM = (uint8)(addr >> 8);
	FSADRL = (uint8)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (uint8)(addr >> 8) 
		&& FSADRL == (uint8)(addr & 0xff))
	{
		FMCR = 0x02;
		_nop_();
		_nop_();
		_nop_();
	}
}


/******************************************
函数名称：PageEraseVA
函数作用：擦除Flash一页(1k)，并校验指定个数的字节，最多校验1024个字节
返回值：无符号8位整数 -- 擦除校验成功返回0，擦除校验失败返回1
参数： 无符号16位整数 addr -- 待擦除的页首地址
       无符号16位整数 verify_num -- 校验字节个数
注：
1、如果给入的参数不是页首地址，则擦除该地址所在页的内容。
   比如给入0x3cdf地址，则实际擦除的是0x3c00地址开始的1k内容。
2、校验从页首地址开始。
3、如果需要校验的字节数<=255，建议用PageEraseVB函数。
*******************************************/
uint8 PageEraseVA(uint16 addr, uint16 verify_num)
{
	uint16 i;
	
	addr = addr & 0xfc00;
	
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	FSADRH = 0x00;
	FSADRM = (uint8)(addr >> 8);
	FSADRL = (uint8)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (uint8)(addr >> 8) 
		&& FSADRL == (uint8)(addr & 0xff))
	{
		FMCR = 0x02;
		_nop_();
		_nop_();
		_nop_();
	}

	for(i=0; i<verify_num; i++)
	{
		if(CBYTE[addr++] != 0)
			return 1;
	}
	return 0;
}


/******************************************
函数名称：PageEraseVB
函数作用：擦除Flash一页(1k)，并校验指定个数的字节，最多校验255个字节
返回值：无符号8位整数 -- 擦除校验成功返回0，擦除校验失败返回1
参数： 无符号16位整数 addr -- 待擦除的页首地址
       无符号8位整数 verify_num -- 校验字节个数
注：
1、如果给入的参数不是页首地址，则擦除该地址所在页的内容。
   比如给入0x3cdf地址，则实际擦除的是0x3c00地址开始的1k内容。
2、校验从页首地址开始。
3、如果需要校验的字节数>255，必须使用PageEraseVA函数。
*******************************************/
uint8 PageEraseVB(uint16 addr, uint8 verify_num)
{
	uint8 i;
	
	addr = addr & 0xfc00;
	
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	FSADRH = 0x00;
	FSADRM = (uint8)(addr >> 8);
	FSADRL = (uint8)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (uint8)(addr >> 8) 
		&& FSADRL == (uint8)(addr & 0xff))
	{
		FMCR = 0x02;
		_nop_();
		_nop_();
		_nop_();
	}

	for(i=0; i<verify_num; i++)
	{
		if(CBYTE[addr++] != 0)
			return 1;
	}
	return 0;
}

/******************************************
函数名称：ByteWrite
函数作用：在Flash指定地址写入数据
返回值：无
参数： 无符号16位整数 addr -- 写入地址
       无符号8位整数 dat -- 写入数据
*******************************************/
void ByteWrite(uint16 addr, uint8 dat)
{
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	*((uint8 xdata *)0x8000) = dat;
	
	FSADRH = 0x00;
	FSADRM = (uint8)(addr >> 8);
	FSADRL = (uint8)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (uint8)(addr >> 8) 
		&& FSADRL == (uint8)(addr & 0xff))
	{
		FMCR = 0x03;
		_nop_();
		_nop_();
		_nop_();
	}
}

/******************************************
函数名称：ByteWriteV
函数作用：在Flash指定地址写入数据，并校验
返回值：无符号8位整数 -- 校验成功返回0，校验失败返回1
参数： 无符号16位整数 addr -- 写入地址
       无符号8位整数 dat -- 写入数据
*******************************************/
uint8 ByteWriteV(uint16 addr, uint8 dat)
{
	FMCR = 0x01;
	_nop_();
	_nop_();
	_nop_();
	
	*((uint8 xdata *)0x8000) = dat;
	
	FSADRH = 0x00;
	FSADRM = (uint8)(addr >> 8);
	FSADRL = (uint8)(addr & 0xff);
	
	FIDR = 0xa5;
	if(FSADRH == 0x00 
		&& FSADRM == (uint8)(addr >> 8) 
		&& FSADRL == (uint8)(addr & 0xff))
	{
		FMCR = 0x03;
		_nop_();
		_nop_();
		_nop_();
	}
	
	if(CBYTE[addr] != dat)
		return 1;
	else
		return 0;
}

/******************************************
函数名称：ArrayWriteA
函数作用：将数组写入Flash指定地址，最多写入1024个字节
返回值：无
参数： 无符号16位整数 addr -- 起始写入地址
       无符号8位整型指针 ptr -- 数组首地址
       无符号16位整型 num -- 写入字节个数
注：
1、如果需要写入的字节数<=255，建议使用ArrayWriteB
*******************************************/
void ArrayWriteA(uint16 addr, uint8 xdata *ptr, uint16 num)
{
	uint16 i;
	
	for(i=0; i<num; i++)
	{
		FMCR = 0x01;
		_nop_();
		_nop_();
		_nop_();
		
		*((uint8 xdata *)0x8000) = ptr[i];
		
		FSADRH = 0x00;
		FSADRM = (uint8)(addr >> 8);
		FSADRL = (uint8)(addr & 0xff);
		
		FIDR = 0xa5;
		if(FSADRH == 0x00 
			&& FSADRM == (uint8)(addr >> 8) 
			&& FSADRL == (uint8)(addr & 0xff))
		{
			FMCR = 0x03;
			_nop_();
			_nop_();
			_nop_();
		}	
		
		addr++;
	}
}

/******************************************
函数名称：ArrayWriteB
函数作用：将数组写入Flash指定地址，最多写入255个字节
返回值：无
参数： 无符号16位整数 addr -- 起始写入地址
       无符号8位整型指针 ptr -- 数组首地址
       无符号8位整型 num -- 写入字节个数
注：
1、如果需要写入的字节数>255，必须使用ArrayWriteA
*******************************************/
void ArrayWriteB(uint16 addr, uint8 xdata *ptr, uint8 num)
{
	uint8 i;
	
	for(i=0; i<num; i++)
	{
		FMCR = 0x01;
		_nop_();
		_nop_();
		_nop_();
		
		*((uint8 xdata *)0x8000) = ptr[i];
		
		FSADRH = 0x00;
		FSADRM = (uint8)(addr >> 8);
		FSADRL = (uint8)(addr & 0xff);
		
		FIDR = 0xa5;
		if(FSADRH == 0x00 
			&& FSADRM == (uint8)(addr >> 8) 
			&& FSADRL == (uint8)(addr & 0xff))
		{
			FMCR = 0x03;
			_nop_();
			_nop_();
			_nop_();
		}	
		
		addr++;
	}
}

/******************************************
函数名称：ArrayWriteVA
函数作用：将数组写入Flash指定地址，最多写入65535个字节,并校验
返回值：无符号8位整数 -- 校验成功返回0，校验失败返回1
参数： 无符号16位整数 addr -- 起始写入地址
       无符号8位整型指针 ptr -- 数组首地址
       无符号16位整型 num -- 写入字节个数
注：
1、如果需要写入的字节数<=255，建议使用ArrayWriteVB
*******************************************/
uint8 ArrayWriteVA(uint16 addr, uint8 xdata *ptr, uint16 num)
{
	uint16 i;
	
	for(i=0; i<num; i++)
	{
		FMCR = 0x01;
		_nop_();
		_nop_();
		_nop_();
		
		*((uint8 xdata *)0x8000) = ptr[i];
		
		FSADRH = 0x00;
		FSADRM = (uint8)(addr >> 8);
		FSADRL = (uint8)(addr & 0xff);
		
		FIDR = 0xa5;
		if(FSADRH == 0x00 
			&& FSADRM == (uint8)(addr >> 8) 
			&& FSADRL == (uint8)(addr & 0xff))
		{
			FMCR = 0x03;
			_nop_();
			_nop_();
			_nop_();
		}
		
		if(CBYTE[addr] != ptr[i])
			return 1;
		
		addr++;
	}
	return 0;
}

/******************************************
函数名称：ArrayWriteVB
函数作用：将数组写入Flash指定地址，最多写入255个字节,并校验
返回值：无符号8位整数 -- 校验成功返回0，校验失败返回1
参数： 无符号16位整数 addr -- 起始写入地址
       无符号8位整型指针 ptr -- 数组首地址
       无符号8位整型 num -- 写入字节个数
注：
1、如果需要写入的字节数>255，必须使用ArrayWriteVA
*******************************************/
uint8 ArrayWriteVB(uint16 addr, uint8 xdata *ptr, uint8 num)
{
	uint8 i;
	
	for(i=0; i<num; i++)
	{
		FMCR = 0x01;
		_nop_();
		_nop_();
		_nop_();
		
		*((uint8 xdata *)0x8000) = ptr[i];
		
		FSADRH = 0x00;
		FSADRM = (uint8)(addr >> 8);
		FSADRL = (uint8)(addr & 0xff);
		
		FIDR = 0xa5;
		if(FSADRH == 0x00 
			&& FSADRM == (uint8)(addr >> 8) 
			&& FSADRL == (uint8)(addr & 0xff))
		{
			FMCR = 0x03;
			_nop_();
			_nop_();
			_nop_();
		}
		
		if(CBYTE[addr] != ptr[i])
			return 1;
		
		addr++;
	}
	return 0;
}

/******************************************
函数名称：ByteRead
函数作用：返回Flash指定地址数据
返回值：无符号8位整数 -- 返回的数据
参数： 无符号16位整数 addr -- 指定的地址
*******************************************/
uint8 ByteRead(uint16 addr)
{
	return CBYTE[addr];
}



