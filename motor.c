/* 
Automatic Balancing Valve
File: motor.c

*/
#include	"AiP8F1016.h"
#include "main.h"

//************************************************************************************
//											BALANCING VALVE MOTOR CONTROL
//************************************************************************************


extern xdata WORD motortarget;
extern xdata WORD currentmotorposition;
extern xdata WORD mtrcountfree;
extern xdata BYTE open;
extern xdata BYTE close;

//vinay
extern xdata int startCalib;
extern xdata char TempError;
extern xdata int testIncr;
extern xdata BYTE testIncrProd;

void MoveToPosition(WORD position)
{
	motor(OFF);
	motortarget = position;
	if(motortarget != mtrcountfree)
		{
		MotorMove();
		do { TimerPoll(); 
		//AiP8F1016:
		if(startCalib == 1)	{	//20211011: may need to test motor current read when power off during close	
		if(testIncr > 1){
		testIncr = 0;
		readMotorCurrentAD();
		//sendpin(motorCurrVAvgdbg);
		//sendpin(mtrcountfree);
		}	}	
		//20210710: 5sec delay for no valve during production
		//else if(startCalib == 2)
		//while(testIncrProd != 1);		
		} while (open || close);
	}
}

void MotorMove(void)
{
	//V0552.5
	//AiP8F1016: Solid Green when balancing
	/*if((startCalib == 2) || (TempError == TRUE))
		TimerStart(LED,FlashLed ,120,TIMER_FOREVER);	//new request: 2hz, 0.5sec flash, ON-0.25s OFF-0.25s
	else if(startCalib == 1)
		TimerStart(LED,FlashLed,340,TIMER_FOREVER);
	else
		TimerStart(LED,FlashLed,800,TIMER_FOREVER);
	*/
	
	// OK move the motor to the value given in the Motor Target
	if(motortarget > currentmotorposition)
	{
		//vinay
		/*TimerReset(2);
		if(startVoP == 1)
		{
			TimerStart(LED,FlashLed,345,TIMER_FOREVER);		//new request: 0.7hz, flash 1.44s, ON-0.72s OFF-0.72s		
		}
		else
		{
			TimerStart(LED,FlashLed,800,TIMER_FOREVER);		//new request: 0.3hz, 3.3s flash, ON-1.66s OFF-1.66s
		}*/		
		motor(CLOSE); // we neeed to close the valve to get to the tar
	}
	else if (motortarget < currentmotorposition)
	{
		//vinay
		/*TimerReset(2);
		if(startVoP == 1)
		{
			TimerStart(LED,FlashLed,345,TIMER_FOREVER);		//new request: 0.7hz, flash 1.44s, ON-0.72s OFF-0.72s				
		}
		else
		{
			TimerStart(LED,FlashLed,800,TIMER_FOREVER);		//new request: 0.3hz, 3.3s flash, ON-1.66s OFF-1.66s			
		}*/	
		motor(OPEN);
	}
	else motor(OFF);
}

void motor(BYTE mtrstate)
{
		//vinay - comment flash timer reset,start and move above
		//TimerReset(2); // reset flash timer
		TimerReset(3);
	switch(mtrstate)
	{
		WORD i;
		
		case OPEN:
			open = TRUE;
			close = FALSE;
			//AiP8F1016:			
			P26 = 0;
			//P16 = 0; // put sensor on
			P24 = 0; 
			P25 = 1; // switch the motor on
			TimerStart(ENDSTOP,EndstopDriver,50,TIMER_FOREVER); // check for stall every 250 count
			break;
		
		case CLOSE:
			open = FALSE;
			close = TRUE;			
			//AiP8F1016:			
			P26 = 0;
			//P16 = 0; // put sensor on
			P25 = 0; 
			P24 = 1; // switch the motor on
			for(i = 0;i<10000;i++);
			TimerStart(ENDSTOP,EndstopDriver,10,TIMER_FOREVER); // check for stall every 250 count
			break;
		
		case OFF:
			TimerReset(3); // OK we must be at the endstop
			if(TempError == FALSE)
				TimerReset(2);      // stop the LED flashing
			P24 = 0;
			P25 = 0;
			for(i=0;i<10000;i++);			
			//AiP8F1016:			
			P26 = 1;
			//P16 = 1;
			open = FALSE;
			close = FALSE;
			//AiP8F1016:
			//P12 = 1; // put LED on
			//P36 = 1;
			P33 = 1;
			currentmotorposition = mtrcountfree;
			break;
		default: // should never get here but belt & braces			
			//AiP8F1016:			
			P26 = 1;
			//P16 = 1;
			P24 = 0;
			P25 = 0;
			open = FALSE;
			close = FALSE;
			currentmotorposition = mtrcountfree;
	}
}
		
		
		
		
		
		
		
		
