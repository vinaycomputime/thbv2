//======================================================
// Main program routine
// - Device name  : AiP8F1016
// - Package type : 
//======================================================
#define		MAIN	1

#include	"AiP8F1016.h"
//#include	"func_def.h"

void Timer0_init();

//void main()
void timer()
{
	cli();          	// disable INT. during peripheral setting
	//port_init();    	// initialize ports
	//clock_init();   	// initialize operation clock
	Timer0_init();  	// initialize Timer0
	sei();          	// enable INT.
	
	// TODO: add your main code here
	
	//while(1)
	//{
	//}
}

//======================================================
// interrupt routines
//======================================================

void INT_Timer0() interrupt 13
{
	// Timer0 interrupt
	P00 = ~P00;
	// TODO: add your code here
}

//======================================================
// peripheral setting routines
// vinay: 1/(16MHz/128) = 8uS*(124+1) = 1ms???
//======================================================

void Timer0_init()
{
	// initialize Timer0
	// 8bit timer, period = 1.000000mS
	T0CR = 0x88;    	// timer setting
	T0DR = 0x7C;    	// period count, decimal 124
	IE2 |= 0x02;    	// Enable Timer0 interrupt
	T0CR |= 0x01;   	// clear counter
}

/*void clock_init()
{
	// internal RC clock (16.000000MHz)
	OSCCR = 0x28;   	// Set Int. OSC
	SCCR = 0x00;    	// Use Int. OSC
}

void port_init()
{
	// initialize ports
	P0IO = 0xFF;    	// direction
	P0PU = 0x00;    	// pullup
	P0OD = 0x00;    	// open drain
	P03DB = 0x00;   	// bit7~6(debounce clock), bit5~0=P35,P06~02 debounce
	P0   = 0x00;    	// port initial value

	P1IO = 0xFF;    	// direction
	P1PU = 0x00;    	// pullup
	P1OD = 0x00;    	// open drain
	P12DB = 0x00;   	// debounce : P23~20, P13~10
	P1   = 0x00;    	// port initial value

	P2IO = 0xFF;    	// direction
	P2PU = 0x00;    	// pullup
	P2OD = 0x00;    	// open drain
	P2   = 0x00;    	// port initial value

	P3IO = 0xFF;    	// direction
	P3PU = 0x00;    	// pullup
	P3OD = 0x00;    	// open drain
	P3   = 0x00;    	// port initial value

	// Set port functions
	P0FSR = 0x00;   	// P0 selection
	P1FSRH = 0x00;  	// P1 selection High
	P1FSRL = 0x00;  	// P1 selection Low
	P2FSR = 0x00;   	// P2 selection
	P3FSR = 0x00;   	// P3 selection
}
*/
