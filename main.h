

/*****************************************************************************
Automatic Balancing Valve
File: main.h
(c) Computime 2016
Author: Phil Smith
Date: 23/08/2016
Version: V0.01
Last Updated: 26/09/2107 (Production Version)
*****************************************************************************/

//************************************************************************************
//											BALANCING VALVE MAIN HEADER
//************************************************************************************



#ifndef _MAIN_H_
#define _MAIN_H_

/*****************************************************************************
System defines
*****************************************************************************/
#define TRUE (1)
#define FALSE (0)
#define MAX_TIMERS 4
#define NULL ( (void *) 0)
#define OPEN  0
#define CLOSE 1
#define ON 2
#define OFF 3
#define RTC 0
#define DTIME 1
#define LED 2
#define ENDSTOP 3
//AiP8F1016:
//#define MTRCURR 2		//To use MTRCURR: assign 4 to LED and enable MTRCURR

//#define BALSTART 0
//#define BALBELOW 1
//#define BALABOVE 2

#define ATVOP 4
#define ATMPP 5
#define BALANCING 6

//AiP8F1016:
#define MAXCOUNTADDR 0x3c00
#define FCALADDR 0x3c02
#define LKBPADDR 0x3c04
#define FONADDR 0x3c06
#define MTRADDR 0x3c08

#define TIMER_ONE_TIME 0
#define TIMER_FOREVER 1
typedef unsigned char BOOL;
typedef unsigned int WORD;
typedef unsigned char BYTE;

/*****************************************************************************
Prototypes
*****************************************************************************/
void readad(void);
void readNTC(void);	//AiP8F1016
void readMotorCurrentAD(void);	//AiP8F1016
void TimerPoll(void);
void TimerInitSW(void);
void TimerStart(BYTE,void(*func) (void), WORD, BYTE);           
void SetPorts(void);
void reatime(void);
void motor(BYTE);
void MotorMove(void);
void TimerReset(BYTE);
void FlashLed(void);
void RealTime(void);
void EndstopDriver(void);
void BalanceSetup(void);
void DoBalance(void);
void EraseData(void);
void WriteFlash(void);
void WriteMaxCount(WORD);
void WriteVOP(WORD);
void WriteCAL(WORD);
void WriteOnTimes(WORD);
void WriteMotorPosition(WORD);
void WriteLKBP(WORD);
WORD ReadMaxCount(void);
WORD ReadVOP(void);
WORD ReadCAL(void);
WORD ReadOnTimes(void);
WORD ReadMotorPosition(void);
WORD ReadLKBP(void);
void disablerst(void);
void redoit(void);
void powerisoff(void);
void reboot(void);
void dobalance(void);
void MoveToPosition(WORD);
void findVOP(void);
void DeltaTime(void);
void PCBtest(void);
void PCBtest1(void);
void LedFlash(BYTE,BYTE);
void clearWDT(void);
void WDTinit(void);
void sendrawtemp(WORD);
void sendtemp(WORD);
void sendpin(WORD);
void sendrawpin(WORD);
void sendbyte(BYTE);
void sendstring(BYTE *message);
//AiP8F1016:
void PageErase(int addr);
int ReadData(int addr);
void WriteData(int addr, int dataval);	


typedef struct
{
void (*callback) (void); 
WORD msecs;
WORD value;
BYTE mode;
BYTE timeout;
}
TIMERITEM;


#endif
