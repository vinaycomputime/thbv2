/* 
Automatic Balancing Valve
File: temperature.c

*/

#include	"AiP8F1016.h"
#include "main.h"
#include "math.h"		//AiP8F1016

//************************************************************************************
//											BALANCING VALVE TEMPERATURE MEASUREMENT
//************************************************************************************


xdata int sensor1 = 0;
xdata int sensor2 = 0;

xdata volatile int flowtemp = 0, returntemp = 0;
xdata volatile int deltat = 0;

xdata volatile int ken1, ken2;

extern xdata BYTE flow, rturn; 
extern xdata char TempError;


code int Temptable[] =
{	// From 0C to 49C (Maybe need to expand to 60C)
		2868,2841,2814,2786,2757,
		2728,2698,2668,2638,2607,
		2574,2541,2509,2476,2442,
		2408,2373,2338,2303,2268,
		2232,2195,2160,2122,2085,
		2047,2010,1972,1933,1895,
		1856,1820,1781,1742,1703,
		1665,1627,1589,1549,1511,
		1473,1436,1399,1364,1324,
		1287,1250,1215,1179,1144,
		1109,1074,1039,1004,969,
		934,899,864,829,794
};

extern xdata int startCalib;

xdata int motorCurrVoltage[138] = {0};	//138 is to stop when force(adc value) starts falling see graph
xdata BYTE currIndex = 0;
//xdata BYTE currIndexLocal = 0;
xdata int motorCurrVoltageAvg = 0;
xdata int motorCurrVAvgdbg = 0;
xdata int valveInstallChk = 0;
extern xdata WORD mtrcountfree;
extern xdata int testIncrAdc;


void readMotorCurrentAD(void)
{
	BYTE adcnt = 0, i;
	int adtemp = 0; // must be int
	long int motorVtg = 0;
	long int temp11 = 0;     // must be long integer
	//int motorCurrVoltageLocal[168];
	//AiP8F1016: Test one time read
		//adcnt = 3;
	do
	{		  //AiP8F1016:
			ADCCRL = 0xC6;	//AN6 for Motor Current 
			while ((ADCCRL | 0xEF)!=0xff);
			adtemp = (ADCDR >> 4);	//20211027: ADC value debug during calibration
			//adtemp = (ADCDRH & 0x0f) | ADCDRL;	//testing
				//adtemp = ADCDRH | (ADCDRL & 0xf0);	//testing
				//adtemp = adtemp >> 4;
			adcnt++;
			//	adcnt--;
			temp11 += adtemp;
					//TimerPoll();
	} while (adcnt!=0);
		adtemp = temp11>>8; // ok average it out by shifting
		//adtemp = temp11/3;
		
		//Verify this????, test: comment Voltage
		//motorVtg = (adtemp*2700);
		//motorCurrVoltage[currIndex] = (motorVtg/4095);	//2.7V system voltage with 12 bit ADc 
		//if(testIncrAdc > 1) {	//tested and skips update first 26 values(5sec to 10sec)
		motorCurrVoltage[currIndex] = adtemp; 	//testing raw adc 
		//motorCurrVoltage[currIndex] = mtrcountfree;		//debug motor count	
		//20211013: Try not updating for 1st 5sec due to variations based on initial force
		if(testIncrAdc > 1) {
		//motorCurrVAvgdbg = motorCurrVoltage[currIndex];
		motorCurrVAvgdbg = adtemp;
		}
		//AiP8F2016: 7 count will yeild around 56sec which is almost Valve fully close timing.
		if(currIndex >= 137)
		{
			currIndex = 0;
			for(i=0; i<138; i++)
			{
				motorCurrVoltageAvg += motorCurrVoltage[i];	
				if(motorCurrVoltage[i] >= 170)
				valveInstallChk++;	 
			}
			motorCurrVoltageAvg = motorCurrVoltageAvg/138; 
		}
		else
		currIndex++;
		
		//20211021: testing
		/*if(currIndexLocal >=167)
		currIndexLocal = 0;
		else
		{
		motorCurrVoltageLocal[currIndexLocal]= adtemp;
		currIndexLocal++;
		}*/	
}

xdata int NTCsensor = 0;	//103AT-2 NTC Thermistor
long int adcValNTC = 0;

void readNTC(void)
{
	int adtemp = 0;     // must be long int

	const float invBeta = 1.00 / 3435.00;   // replace "Beta" with beta of thermistor

  const  float adcMax = 4095.00;
  const float invT0 = 1.00 / 298.15;   // room temp in Kelvin

  int i, numSamples = 256;
  float  Kelvin;

  for (i = 0; i < numSamples; i++)
   {
 			ADCCRL = 0xC4;
			while ((ADCCRL | 0xEF)!=0xff);
			adtemp = (ADCDR >> 4);
      adcValNTC = adcValNTC + adtemp;
      //delay(100);
		 //TimerPoll();	//testing
   }
  adcValNTC = adcValNTC/numSamples;
  Kelvin = 1.00 / (invT0 + invBeta*(log ( adcMax / (float) adcValNTC - 1.00)));
  NTCsensor = (Kelvin - 273.15)*100; 
}	

xdata int sensor1ADC, sensor2ADC;

void readad(void)
{
	// read ad11
	BYTE adcnt = 0;
	int adtemp, adtemp1 = 0; // must be int
	long int temp11 = 0;     // must be long int
	do
	{ // take 256 readings for sensor1
		  //AiP8F1016:
			//ADCCRL = 0x8b;
			//ADCCRL = 0xCb;
			ADCCRL = 0xC4;
			//while(!(ADCCRL & 0x10));	// wait ADC busy
			//adtemp = (ADCDR >> 4);	// read ADC, No ADCDR in AiP8F1016
			while ((ADCCRL | 0xEF)!=0xff);
			adtemp = (ADCDR >> 4);
			//adtemp = ADCDRH | (ADCDRL >> 4);	// read ADC, High: MSB: 11:4, Low: MSB: 3:0 (refer Datasheet for details)
			//adtemp = ADCDRH | ADCDRL;
			//adtemp = adtemp >> 4;
			//adtemp = ADCDRH | (ADCDRL & 0xf0);
			//adtemp = adtemp >> 4;
			sensor1ADC = adtemp; 	//AiP8F1016: capture ADC value to debug 
			adcnt++;
			temp11 += adtemp;
	//		TimerPoll();
		} while (adcnt!=0);
		adtemp = temp11>>8; // ok average it out by shifting
		ken1 = adtemp;
		sensor1ADC = adtemp; 	//AiP8F1016: capture ADC value to debug 
	for(adcnt = 0; adcnt<70;adcnt++) // find our atod value in the table
		{
			if (adtemp >= Temptable[adcnt])
			{
				sensor1 = adcnt*100; // need to have 2 decimal places (2500 equals 25.00C)
				adtemp1 = ((Temptable[adcnt] - adtemp) *100) / (Temptable[adcnt]-Temptable[adcnt+1]); // get the decimal amount
				sensor1 += adtemp1; // add them (2500 + 75 equals 2575 equals 25.75C
				adcnt = 70; // we found it so end the table search
			}		
		}
	adcnt =0;
	adtemp =0;
	adtemp1=0;
	temp11=0;
	do
	{// take 256 readings for sensor2 just a duplicate of sensor1 above
			//AiP8F1016:
			//ADCCRL = 0x8c;
			//ADCCRL = 0xCc;
		  ADCCRL = 0xC3;
			//while(!(ADCCRL & 0x10));	// wait ADC busy
			//adtemp = (ADCDRH << 8) | ADCDRL;	// read ADC, No ADCDR in AiP8F1016
			//adtemp = (ADCDRH & 0x0f) | ADCDRL;	// read ADC, High: LSB: 11:8, Low: LSB: 7:0 (refer Datasheet for details)
			while ((ADCCRL | 0xEF)!=0xff);
			adtemp = (ADCDR >> 4);
			adcnt++;
			temp11 += adtemp;
//		  TimerPoll();
		} while (adcnt!=0);
		adtemp = temp11>>8;
		//adtemp+=14;
		ken2=adtemp;
		sensor2ADC = adtemp; 	//AiP8F1016: capture ADC value to debug
	for(adcnt = 0; adcnt<70;adcnt++)
		{
			if (adtemp >= Temptable[adcnt])
			{
				sensor2 = adcnt*100;
				adtemp1 = ((Temptable[adcnt] - adtemp) *100) / (Temptable[adcnt]-Temptable[adcnt+1]);
				sensor2 += adtemp1;
				adcnt = 70;
			}		
		}
		//Vinay, 0552.6: added startCalib
		//AiP8F1016: Not included in PRD
		/*if(sensor1<1000 || sensor1>3600 || sensor2<1000 || sensor2>3600)
		{
			//TimerReset(2);
			if(TempError == FALSE)
			{
				TempError = TRUE;
				TimerStart(LED,FlashLed,120,TIMER_FOREVER);	//new request: 2hz, 0.5sec flash, ON-0.25s OFF-0.25s
			}
		}
		else
		{
			if(TempError == TRUE)
			{
				TempError = FALSE;
				TimerReset(2);
				//AiP8F1016:
				//P12 = 1; // put LED on
				//P36 = 1;
				P33 = 1;
			}
		}*/

	if(sensor1-100 > sensor2)
	{
		flow = TRUE;
		rturn = FALSE;
	}
	else if (sensor2-100 > sensor1)
	{
		flow = FALSE;
		rturn = TRUE;
	}
//	else     // changed may cause a problem if we switch from a flow / return to neither
//	{
//		flow = FALSE;
//		rturn = FALSE;
//	}	
if (flow == TRUE)
{
	
	flowtemp = sensor1;
	returntemp = sensor2;
	deltat = flowtemp - returntemp;
}
else if (rturn == TRUE)
	{
	flowtemp = sensor2;
	returntemp = sensor1;
	deltat = flowtemp - returntemp;
	}
	//AiP8F1016:
	//if(sensor1 > 1800 && sensor1 < 2800)
	//P33 = 1;	
	//readNTC();
	//if(NTCsensor > 2000 && NTCsensor < 3000)	
	//P33 = 1;	
	//if(adcValNTC>1500) //if(adcValNTC>1500 && adcValNTC<2500)	//reading greater than 2500, why??
		
}
